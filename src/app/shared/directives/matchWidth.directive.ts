import {
    Directive, ElementRef, AfterViewChecked,
    Input, HostListener
} from '@angular/core';

/* THIS DIRECTIVE MATCHES THE HEIGHT OF ALL CARDS IN A LIST, EVEN IF THEY ARE RESPONSIVE */
/* HOW TO USE:
    // THE DIRECTIVE WILL BE PLACED IN THE ELEMENTTHAT SURROUNDS THE LIST(EL CALLING NGFOR)
    // THE DIRECTIVES INPUT WILL BE A CLASS NAME THAT IS PLACED ON EVERY ELEMENT IN LIST
*/

@Directive({
    selector: '[appMatchWidth]'
})
export class MatchWidthDirective implements AfterViewChecked {
    // class name to match width
    @Input()
    appMatchWidth: string;

    constructor(private el: ElementRef) {
    }

    ngAfterViewChecked() {
        // call our matchWidth function here later
        this.matchWidth(this.el.nativeElement, this.appMatchWidth);
    }

    @HostListener('window:resize')
    onResize() {
        // call our matchWidth function here later
        this.matchWidth(this.el.nativeElement, this.appMatchWidth);
    }

    matchWidth(parent: HTMLElement, className: string) {
        // match width logic here

        if (!parent) { return; }

        const children = parent.getElementsByClassName(className);

        if (!children) { return; }

        // reset all children width
        Array.from(children).forEach((x: HTMLElement) => x.style.width = 'initial');

        // gather all widths
        const itemWidths = Array.from(children).map(x => x.getBoundingClientRect().width);

        // find the max width
        const maxWidth = itemWidths.reduce((prev, curr) => curr > prev ? curr : prev);

        // apply max width to each element
        Array.from(children).forEach((x: HTMLElement) => x.style.width = `${maxWidth}px`);

    }
}
