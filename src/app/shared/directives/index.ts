import { MatchHeightDirective } from './matchHeight.directive';
import { MatchWidthDirective } from './matchWidth.directive';


export const directives = [
    MatchHeightDirective,
    MatchWidthDirective,
];
