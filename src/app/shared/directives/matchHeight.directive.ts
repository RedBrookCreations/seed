import {
    Directive, ElementRef, AfterViewChecked,
    Input, HostListener
} from '@angular/core';

/* THIS DIRECTIVE MATCHES THE HEIGHT OF ALL CARDS IN A LIST, EVEN IF THEY ARE RESPONSIVE */
/* HOW TO USE:
    // THE DIRECTIVE WILL BE PLACED IN THE ELEMENTTHAT SURROUNDS THE LIST(EL CALLING NGFOR)
    // THE DIRECTIVES INPUT WILL BE A CLASS NAME THAT IS PLACED ON EVERY ELEMENT IN LIST
*/

@Directive({
    selector: '[appMatchHeight]'
})
export class MatchHeightDirective implements AfterViewChecked {
    // class name to match height
    @Input()
    appMatchHeight: string;

    constructor(private el: ElementRef) {
    }

    ngAfterViewChecked() {
        // call our matchHeight function here later
        this.matchHeight(this.el.nativeElement, this.appMatchHeight);
    }

    @HostListener('window:resize')
    onResize() {
        // call our matchHeight function here later
        this.matchHeight(this.el.nativeElement, this.appMatchHeight);
    }

    matchHeight(parent: HTMLElement, className: string) {
        // match height logic here

        if (!parent) { return; }

        const children = parent.getElementsByClassName(className);

        if (!children) { return; }

        // reset all children height
        Array.from(children).forEach((x: HTMLElement) => x.style.height = 'initial');

        // gather all heights
        const itemHeights = Array.from(children).map(x => x.getBoundingClientRect().height);

        // find the max height
        const maxHeight = itemHeights.reduce((prev, curr) => curr > prev ? curr : prev);

        // apply max height to each element
        Array.from(children).forEach((x: HTMLElement) => x.style.height = `${maxHeight}px`);

    }
}
