import { Injectable, TemplateRef, Component, Inject } from '@angular/core';
/* tslint:disable-next-line */
import { MatSnackBar, MatBottomSheetConfig, MatBottomSheetRef, MatDialogConfig, MatDialog, MatBottomSheet, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ComponentType } from '@angular/core/src/render3';

@Injectable({
  providedIn: 'root'
})
export class UiService {

  constructor(
    private snackbar: MatSnackBar,
    public modal: MatDialog,
    public bottomSheet: MatBottomSheet
  ) { }
  /**
  * Paramaters are a string, a delay, and an optional label.
  * @param message
  * @param delay
  * @param label
  */
  openSnackbar(message: string, delay: number = 2000, label?: string) {
    !!!label ? label = '' : label = label;
    this.snackbar.open(message, label, { duration: delay });
  }

  // OPEN A MODAL
  /**
  * Takes a component or template, an object with data, and an optional callBack method.
  * The component or template is what is what the modal is based on.  The data is data that can be
  * shared with the component or template.  The callback is a method that is run when the modal is closed
  * on the seperator returning an object with two properties
  * @param componentOrTemplate
  * @param data
  * @param afterClosed
  */
  openModal(
    title: string,
    content: string,
    ) {

    const config: MatDialogConfig = {
        data: { title: title, content: content }
    };

    const modalRef = this.modal.open(ModalComponent, config);

    // Modal is Closed
    modalRef
        .afterClosed()
        .subscribe(result => {
            // afterClosed(result);
        })
        .unsubscribe();

    // Modal Backdrop is Clicked
    modalRef
        .backdropClick()
        .subscribe(result => {
            // backdropClicked(result);
        })
        .unsubscribe();
  }

  // OPEN BOTTOM SHEET
    /**
  * Takes a component or template, an object with data, and an optional callBack method.
  * The component or template is what is what the modal is based on.  The data is data that can be
  * shared with the component or template.  The callback is a method that is run when the modal is closed
  * on the seperator returning an object with two properties
  * @param componentOrTemplate
  * @param data
  * @param afterClosed
  */
  openBottomSheet(
      template: TemplateRef<any>,
      data?,
      afterClosed?: (result) => any,
      backdropClicked?: (result) => any) {

    const config: MatBottomSheetConfig = {
        data: { ...data }
    };

    const bottomSheetRef: MatBottomSheetRef = this.bottomSheet.open(template, config);

    bottomSheetRef
        .afterDismissed()
        .subscribe(result => {
            afterClosed(result);
        })
        .unsubscribe();

    bottomSheetRef
        .backdropClick()
        .subscribe(result => {
            backdropClicked(result);
        })
        .unsubscribe();
  }
}


/* MODAL COMPONENT SHOWN */
@Component({
  selector: 'app-modal',
  template: `
  <h2 mat-dialog-title>{{ data.title }}</h2>

  <mat-dialog-content>{{ data.content }}</mat-dialog-content>

  <mat-dialog-actions>
      <button class="mat-raised-button"(click)="closeDialog()">Close</button>
  </mat-dialog-actions>`,
})
export class ModalComponent {

  constructor(
      public dialogRef: MatDialogRef<ModalComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  closeDialog(): void {
      this.dialogRef.close();
  }

}

/* BOTTOM SHEET COMPONENT SHOWN */
@Component({
  selector: 'app-bottom-sheet',
  template: ``,
})
export class BottomSheetComponent {
  constructor(private bottomSheetRef: MatBottomSheetRef<BottomSheetComponent>) {}

  dismissBottomSheet(): void {
      this.bottomSheetRef.dismiss();
  }
}
