import { trigger, animate, transition, style, state, keyframes } from '@angular/animations';

export const slideInOutAnimation =
    trigger('slideInOutAnimation', [
        transition(':enter', [
            style({ transform: 'translate(-100%)'}),
            animate('200ms')
        ]),

        transition(':leave', [
            style({ transform: 'translate(0%)'}),
            animate('200ms')
        ])
    ]);

export const slideDownUpAnimation =
    trigger('slideInOutAnimation', [
        transition(':enter', [
            style({ transform: 'translateY(-100%)'}),
            animate('200ms')
        ]),

        transition(':leave', [
            style({ transform: 'translateY(0%)'}),
            animate('200ms')
        ])
    ]);
