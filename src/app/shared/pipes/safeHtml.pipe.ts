import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
/**
 * Takes a HTML string and displays it in the DOM properly santized
 * @returns {SafeHtml}
 * @example 
 * <div [innerHTML]='user?.bio | safeHtml'></div>
 */
@Pipe({ name: 'safeHtml' })
export class SafeHtmlPipe implements PipeTransform {
    constructor(private sanitizer: DomSanitizer) { }
    transform(html): SafeHtml {
        if (!html) return;
        return this.sanitizer.bypassSecurityTrustHtml(html);
    }
}
