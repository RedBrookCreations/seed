import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe, DatePipe } from '@angular/common';

export type DataType = 'currency' | 'date' | 'percentage' | 'number';

/**
 * Global Formatter for Currency, Date, Percentage, and Numeric Values displayed
 * @example
 * <span>{{dollarAmt | jcFormat : 'currency' : 0}}
 */
@Pipe({
    name: 'jcFormat'
})
export class FormatPipe implements PipeTransform {
    transform(value: string, dataType: DataType, format?: string): string {
        if ((typeof value === 'undefined') || (value === null)) {
            return '---';
        }
        switch (dataType) {
            case 'currency':
                const parsedCurrency: number = parseFloat(value);
                let retVal: string;
                if (isNaN(parsedCurrency)) {
                    return format === 'number' ? `$${Math.abs(parsedCurrency).toFixed(+format)}` : '$0.00';
                } else {
                    if (parsedCurrency < 0) {
                        retVal = this.formatCommas(typeof format === 'number'
                            ? Math.abs(parsedCurrency).toFixed(+format)
                            : Math.abs(parsedCurrency).toFixed(2));
                        return `(${retVal})`;
                    } else {
                        retVal = this.formatCommas(typeof format === 'number'
                            ? parsedCurrency.toFixed(+format)
                            : parsedCurrency.toFixed(2));
                        return `$${retVal}`;
                    }
                }
            case 'date':
                const parsedDate = Date.parse(value);
                const dateFormat = format ? format : 'MM/DD/YYYY';
                return new DatePipe('en-US').transform(new Date(parsedDate), dateFormat);
            case 'percentage':
                const parsedPercent = parseFloat(value);
                if (isNaN(parsedPercent)) {
                    return `0.00%`;
                } else {
                    return parsedPercent.toString().indexOf('.') > -1
                        ? parsedPercent.toFixed(!!format ? parseInt(format, 10) : 1).toString() + '%'
                        : parsedPercent.toFixed(!!format ? parseInt(format, 10) : 0).toString() + '%';
                }
            case 'number':
                const parsedNum: number = parseFloat(value);
                if (isNaN(parsedNum)) {
                    return '0.00';
                } else {
                    return new DecimalPipe('en-US').transform(parsedNum, '1.0-2');
                }
            default:
                return value;
        }
    }

    formatCommas(value: string): string {
        const pieces: string[] = value.split('.');
        pieces[0] = pieces[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        return pieces.join('.');
    }
}