import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as CoreModel from '@core/models';
import * as CoreAction from '@core/store/actions';
import * as UISelector from '@core/store/selectors/ui.selectors';

@Component({
  selector: 'app-global-button',
  templateUrl: './global-button.component.html',
  styleUrls: ['./global-button.component.scss']
})

export class GlobalButtonComponent implements OnInit {
  @Output() optionsToggle = new EventEmitter<boolean>();
  optionsShowing$: Observable<boolean>;

  optionsShowing: boolean;

  constructor() { }

  ngOnInit() {
    this.optionsShowing = false;
  }

  toggle() {
    this.optionsShowing = !this.optionsShowing;
    this.optionsToggle.emit(this.optionsShowing);
  }
}
