import { Component, OnInit, Inject } from '@angular/core';

import { FieldConfig } from '@rbcForms/models';
import * as Field from '@scheduling/models/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-form-modal',
  template: `
    <div fxLayout="column" fxLayoutAlign="center start" fxLayoutGap="10px">

        <h1 mat-dialog-title>{{ data.title }}</h1>
        <app-form-group mat-dialog-content [config]="data.formConfig" (submit)="formChanged($event)"></app-form-group>

        <div mat-dialog-actions>
            <button mat-raised-button color="primary" [disabled]="!valid" (click)="submit()">
              {{ data.buttonSubmitText }}
            </button>
        </div>
    </div>
  `,
  styles: [`
  `]
})
export class FormModalComponent implements OnInit {
  clearance = 1;
  config:  FieldConfig[];
  form: any;
  valid = false;

  constructor(
    private dialogRef: MatDialogRef<FormModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  formChanged(value) {
    this.form = value;
    this.valid = this.form.valid;
  }

  submit() {
    this.dialogRef.close(this.form.value);
  }
}
