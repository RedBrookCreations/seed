import { FormCheckBoxComponent } from './checkbox.component';
import { FormDatePickerComponent } from './date-picker.component';
import { FormInputComponent } from './input.component';
import { FormSelectComponent } from './select.component';
import { FormTextAreaComponent } from './text-area.component';
import { FormTitleComponent } from './title.component';
import { FormModalComponent } from './form-modal.component';

export * from './checkbox.component';
export * from './date-picker.component';
export * from './input.component';
export * from './select.component';
export * from './text-area.component';
export * from './title.component';
export * from './form-modal.component';

export const formFieldComponents = [
	FormDatePickerComponent,
	FormInputComponent,
	FormSelectComponent,
	FormTextAreaComponent,
	FormCheckBoxComponent,
	FormTitleComponent,
	FormModalComponent,
];
