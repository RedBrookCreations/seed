import { Component, HostBinding, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FieldConfig, Field, DisplayConfig, InputConfig, Extras } from '../models';

@Component({
  selector: 'app-checkbox',
  template: `
    <div [formGroup]="group">
      <mat-checkbox
          [formControlName]="config.formControlName"
          [disabled]="disabled">
              {{ placeholder }}
      </mat-checkbox>
    </div>
  `,
  styles: [`
    .mat-checkbox, .label, .mat-checkbox-layout, .mat-checkbox-label {
      line-height: 2 !important;
      margin: 5px !important;
    }
    .mat-checkbox-label {
      line-height: 2 !important;
    }
  `],
})
export class FormCheckBoxComponent implements Field {
  @HostBinding('style.flex') public flex;
  @HostBinding('style.lineHeight') public lineHeight = '2';
  @HostBinding('style.marginLeft') public marginLeft = '5px';
  @HostBinding('style.marginRight') public marginRight = '5px';
  @HostBinding('style.marginBottom') public marginBottom = '5px';
  @HostBinding('style.marginTop') public marginTop = '5px';

  @Input() group: FormGroup;
  @Input() config: FieldConfig;
  @Input() inputConfig: InputConfig;
  @Input() displayConfig: DisplayConfig;
  @Input() extras: Extras;

  get value() { return this.group.get(`${this.config.formControlName}`).value; }
  get disabled() { return this.config.disabled; }
  get placeholder() { return this.config.label; }
  get labelPosition() { return this.displayConfig.labelPosition; }
}
