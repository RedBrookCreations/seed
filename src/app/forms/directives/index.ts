import { CreateFieldComponentDirective } from './create-field-component.directive';

export * from './create-field-component.directive';

export const directives = [
    CreateFieldComponentDirective
];
