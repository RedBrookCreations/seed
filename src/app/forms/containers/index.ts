import { FormGroupContainerComponent } from './form-group.component';

export * from './form-group.component';

export const containers = [
    FormGroupContainerComponent,
];
