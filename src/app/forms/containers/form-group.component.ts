import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { FieldConfig, DisplayConfig, ErrorConfig, Extras } from '../models';

@Component({
  selector: 'app-form-group',
  template: `
    <form
      class="field"
      fxLayout="row wrap"
      [formGroup]="form"
      (submit)="handleSubmit($event)">
      <ng-container *ngFor="let field of config;"
        appCreateFieldComponent
        [config]="field"
        [group]="form">
      </ng-container>
    </form>
  `,
  styles: [`
    .field {
      margin-bottom: 10px;
      width: 100%;
    }
  `]
})
export class FormGroupContainerComponent implements OnChanges, OnInit {
  @Input()
  config: FieldConfig[];

  @Output()
  submit: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  changes: EventEmitter<any> = new EventEmitter<any>();

  form: FormGroup;

  get controls() { return this.config; }
  get valid() { return this.form.valid; }
  get value() { return this.form.value; }

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.createGroup();
    this.form.valueChanges.subscribe(changes => {
      if (this.valid) {
        this.submit.emit(this.form);
      }
    });

  }

  ngOnChanges() {
    if (this.form) {
      const controls = Object.keys(this.form.controls);
      const configControls = this.controls.map((item) => item.formControlName);

      controls
        .filter((control) => !configControls.includes(control))
        .forEach((control) => this.form.removeControl(control));

      configControls
        .filter((control) => !controls.includes(control))
        .forEach((name) => {
          const config = this.config.find((control) => control.formControlName === name);
          this.form.addControl(name, this.createControl(config));
        });


    }
  }

  createGroup() {
    const group = this.fb.group({});
    this.controls.forEach(control => group.addControl(control.formControlName, this.createControl(control)));
    this.controls.forEach(control => group.setValidators(control.crossFieldValidators));
    return group;
  }

  createControl(config: FieldConfig) {
    const { value, disabled, validators } = config;
    return this.fb.control({ disabled, value }, validators);
  }

  handleSubmit(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    this.submit.emit(this.value);
  }

  setDisabled(name: string, disabled: boolean) {
    if (this.form.controls[name]) {
      const method = disabled ? 'disable' : 'enable';
      this.form.controls[name][method]();
      return;
    }

    this.config = this.config.map((item) => {
      if (item.formControlName === name) {
        item.disabled = disabled;
      }
      return item;
    });
  }

  setValue(name: string, value: any) {
    this.form.controls[name].setValue(value, {emitEvent: true});
  }

}

