import { firebase } from '@firebase/app';
import { Permissions } from './permissions.model';

export interface AuthState {
    uid: string;
}

export interface FirebaseUser {
    displayName?: any;
    email: any;
    emailVerified: boolean;
    isAnonymous: boolean;
    metadata: firebase.auth.UserMetadata;
    phoneNumber: string | null;
    photoURL: any;
    providerId: any;
    uid: string;
}

export interface EmailLoginData {
    email: string;
    password: string;
}
