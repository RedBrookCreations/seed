export interface UserEntity {
	id: string;
	data: UserData;
}

export interface UserData {
	id: string;

	// permissions
	role: string;
	clearance: number;
	keys: string[];

	// Identity
	email: string;
	phone: string;

	// Information
	description: string;
	profilePhotoUrl: string;
	coverPhotoUrl: string;
	title: string;
	name: string;
	gender: string;
	race: string;
	age: number;
}

export interface UserNotificationsEnabled {
	emailEnabled: boolean;
	smsEnabled: boolean;
	pushEnabled: boolean;
}

export interface UserInformation {
	name: string;
	description: string;
}

export interface UserDemographic {
	age: number;
	gender: string;
	race: string;
}
