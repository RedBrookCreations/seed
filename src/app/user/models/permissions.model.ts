export interface Permissions {
    role: string;
    clearance:  number;
    groups: string[];
}
