import {
Component,
OnInit,
Input,
Output,
EventEmitter,
OnChanges,
ChangeDetectionStrategy,
} from '@angular/core';

import { MatTableDataSource } from '@angular/material';

// Model Imports
import * as Model from '@user/models';

@Component({
    selector: 'app-list-user',
    templateUrl: './list-user.component.html',
    styleUrls: ['./list-user.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListUserComponent implements OnChanges {
    @Input() users: Model.UserData[];
    @Input() permissions: Model.Permissions;

    @Output() userClicked = new EventEmitter<Model.UserData>();

    // Support RBC Form
    loading = false;
    cards = true;
    table = false;
    displayedColumns: string[] = ['name', 'email', 'age', 'demographic', 'role', 'edit'];
    dataSource;

    constructor() {}

    ngOnChanges() {
        this.dataSource = new MatTableDataSource(this.users);
    }

    onClick(user) {
        this.userClicked.emit(user);
    }

    viewCards() {
        this.cards = true;
        this.table = false;
    }

    viewTable() {
        this.cards = false;
        this.table = true;
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
}


