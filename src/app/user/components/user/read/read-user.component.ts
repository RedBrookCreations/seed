import {
	Component,
	OnInit,
	Input,
	Output,
	EventEmitter,
	ChangeDetectionStrategy,
} from '@angular/core';

// Model Imports
import * as Model from '@user/models';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
	selector: 'app-read-user',
	templateUrl: './read-user.component.html',
	styleUrls: ['./read-user.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReadUserComponent implements OnInit {
	@Input() user: Model.UserData;
	@Input() permissions: Model.Permissions;
	@Input() currentId: string;

	@Output() route = new EventEmitter<string>();
	@Output() update = new EventEmitter<any>();

	profilePhoto;
	coverPhoto;
	currentTab: string;
	// tabs = ['Profile', 'Posts', 'Calendar', 'Pictures', 'Videos', 'Settings'];
	tabs = ['About', 'Settings'];
	constructor() {}

	ngOnInit() {
		this.currentTab = 'Settings';
	}

	changeTab(tab) {
		this.currentTab = tab;
	}

	updateInformation(event) {
		this.update.emit(event);
	}
	updatePermissions(event) {
		this.update.emit(event);
	}
	updateDemographic(event) {
		this.update.emit(event);
	}
	updateNotifications(event) {
		this.update.emit(event);
	}
}
