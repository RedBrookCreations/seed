import {
Component,
OnInit,
Input,
Output,
EventEmitter,
ChangeDetectionStrategy,
} from '@angular/core';

// Model Imports
import * as Model from '@user/models';

@Component({
    selector: 'app-create-user',
    templateUrl: './create-user.component.html',
    styles: ['./create-user.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateUserComponent implements OnInit {
    @Input() currentUser: Model.UserData;
    @Input() permissions: Model.Permissions;

    @Output() createUser = new EventEmitter<any>();

    // Support RBC Form
    form: any;
    valid = false;

    constructor() {}

    ngOnInit() {

    }

    formChanged(form) {
        this.form = form;
        this.valid = this.form.valid;
    }

    create() {
        const data: any = {
        };
        this.createUser.emit(event);
    }
}


