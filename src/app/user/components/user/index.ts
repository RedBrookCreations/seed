import { CreateUserComponent } from './create/create-user.component';
import { ReadUserComponent } from './read/read-user.component';
import { UpdateUserComponent } from './update/update-user.component';
import { ListUserComponent } from './list/list-user.component';

export const userComponents = [
    CreateUserComponent,
    ReadUserComponent,
    UpdateUserComponent,
    ListUserComponent,
];
