import {
	Component,
	OnInit,
	Input,
	Output,
	EventEmitter,
	ChangeDetectionStrategy,
} from '@angular/core';

// Model Imports
import * as Model from '@user/models';

@Component({
	selector: 'app-update-user',
	templateUrl: './update-user.component.html',
	styleUrls: ['./update-user.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateUserComponent implements OnInit {
	@Input() user: Model.UserData;
	@Input() permissions: Model.Permissions;

	@Output() updateNotifications = new EventEmitter<any>();
	@Output() updateInformation = new EventEmitter<any>();
	@Output() updatePermissions = new EventEmitter<any>();
	@Output() updateDemographic = new EventEmitter<any>();

	constructor() {}
	ngOnInit() {}

	updateUserInformation(informationFormData) {
		this.updateInformation.emit({
			id: this.user.id,
			data: informationFormData,
		});
	}

	updateUserPermissions(permissionsFormData) {
		this.updatePermissions.emit({
			id: this.user.id,
			data: permissionsFormData,
		});
	}

	updateUserDemographic(demographicFormData) {
		this.updateDemographic.emit({
			id: this.user.id,
			data: demographicFormData,
		});
	}
}
