export * from './auth';

import { userComponents } from './user';
import * as Auth from './auth';

export const components = [
    ...userComponents,
    Auth.ForgotComponent,
    Auth.LoginComponent,
    Auth.RegisterComponent,
    Auth.AdvancedRegisterComponent
];
