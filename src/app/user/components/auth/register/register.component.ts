import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

import * as Model from '@user/models';

/* RBC Forms */
import * as Field from '@user/forms';
import * as Form from '@user/forms/groups';
import { FieldConfig } from '@rbcForms/models';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @Input() permissions;
  @Output() register = new EventEmitter<any>();

  // Profile Form
  profileFormConfig: FieldConfig[];
  profileFormValid: boolean;
  profileForm: any;

  // Permissions Form
  PermissionsForm = new Form.PermissionsForm();
  permissionsFormConfig: FieldConfig[];
  permissionsFormValid: boolean;
  permissionsForm: any;

  // Auth Form
  authFormConfig: FieldConfig[] = [];
  authFormValid = false;
  authForm: any;

  valid = false;

  constructor() { }

  ngOnInit() {

    if (!!this.permissions) {
      this.profileFormValid = this.permissions.role === 'admin' ? false : true;
      this.permissionsFormValid = this.permissions.role === 'admin' ? false : true;
    }
    this.authFormConfig = [
      Field.nameField,
      Field.emailField,
      Field.passwordField,
      Field.confirmPasswordField,
    ];

    this.permissionsFormConfig = this.PermissionsForm.config;

    this.profileFormConfig = [];
  }

  authFormChanged(formData) {
    this.authForm = formData;
    this.authFormValid = this.authForm.valid;
    this.valid = this.authFormValid;
  }
  permissionsFormChanged(formData) {
    this.permissionsForm = formData;
    this.permissionsFormValid = this.permissionsForm.valid;
    this.valid = this.permissionsFormValid;
  }
  profileFormChanged(formData) {
    this.profileForm = formData;
    this.profileFormValid = this.profileForm.valid;
    this.valid = this.profileFormValid;
  }

  isValid() {
    return (this.profileForm.valid === true &&
            this.authForm.valid === true &&
            this.profileForm.valid === true)
              ? true
              : false;
  }

  registerUser() {
    const data: any = {
      auth: this.authForm.value,
      permissions: {
        clearance: 1,
        keys: ['customer'],
        role: 'customer'
      },
    };
    if (!!this.permissionsForm) { data.permissions = this.permissionsForm.value; }
    console.log(data);
    this.register.emit(data);
  }

}
