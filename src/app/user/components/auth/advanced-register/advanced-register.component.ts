import {
Component,
OnInit,
Input,
Output,
EventEmitter
} from '@angular/core';

import * as Model from '@user/models';

import * as Animations from '@shared/animations';

import * as Field from '@user/forms';
import * as Form from '@user/forms/groups';
import { FieldConfig } from '@rbcForms/models';

@Component({
    selector: 'app-advanced-register',
    animations: [ Animations.slideInOutAnimation ],
    templateUrl: './advanced-register.component.html',
    styleUrls: ['./advanced-register.component.scss']
})
export class AdvancedRegisterComponent implements OnInit {
    @Output() register = new EventEmitter<any>();

    nameForm: FieldConfig[] = [ Field.nameField ];
    nameFormValid = false;

    ageForm: FieldConfig[] = [ Field.ageField ];
    ageFormValid = false;

    genderForm: FieldConfig[] = [ Field.genderField ];
    genderFormValid = false;

    raceForm: FieldConfig[] = [ Field.raceField ];
    raceFormValid = false;

    authForm: FieldConfig[] = [ Field.emailField, Field.passwordField ];
    authFormValid = false;

    currentForm = 1;

    constructor() { }

    ngOnInit() {

    }

    next() { this.currentForm++; }
    back() { this.currentForm--; }

    nameFormChanged(form) {
        this.nameFormValid = form.valid;
        Field.nameField.value = form.value.name;
    }

    ageFormChanged(form) {
        this.ageFormValid = form.valid;
        Field.ageField.value = form.value.age;
    }

    genderFormChanged(form) {
        this.genderFormValid = form.valid;
        Field.genderField.value = form.value.gender;
    }

    raceFormChanged(form) {
        this.raceFormValid = form.valid;
        Field.genderField.value = form.value.race;
    }

    authFormChanged(form) {
        this.authFormValid = form.valid;
    }

    registerUser() {
        const data: any = {
            auth: {
                email: Field.emailField.value,
                password: Field.passwordField.value
            },
            permissions: {
                clearance: 1,
                keys: ['customer'],
                role: 'customer'
            },
            userData: {
                name: Field.nameField.value,
                age:  Field.ageField.value,
                gender: Field.genderField.value,
                race: Field.raceField.value,
            }
        };

        console.log(data);

        // this.register.emit(data);
    }

}
