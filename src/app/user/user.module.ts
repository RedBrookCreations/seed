import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import {
	FormsModule,
	ReactiveFormsModule,
} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

// NGRX
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Routes
import { ROUTES } from './containers/routes';

// Components
import * as Component from './components';

// Containers
import * as Container from './containers';

// Form
import { forms } from './forms';
import { RBCFormsModule } from '../forms/forms.module';
import { formFieldComponents } from '../forms/components';

// Modules
import { SchedulingModule } from '@scheduling/scheduling.module';
import { PermissionsFormComponent } from './forms/permissions-form/permissions-form.component';

@NgModule({
	declarations: [
		...Container.containers,
		...Component.components,
		...forms,
	],
	imports: [
		RBCFormsModule,
		SharedModule,
		RouterModule.forChild(ROUTES),
		FormsModule,
		ReactiveFormsModule,
		CommonModule,
		SchedulingModule,
	],
	entryComponents: [...formFieldComponents],
	providers: [],
})
export class UserModule {}
