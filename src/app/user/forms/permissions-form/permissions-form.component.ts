import {
	Component,
	OnInit,
	Output,
	EventEmitter,
	ChangeDetectionStrategy,
} from '@angular/core';

// RxJS
import { Observable } from 'rxjs';

// Models
import * as Model from '../../models';

// Store
import { Store } from '@ngrx/store';
import * as Selector from '../../store/selectors';
import * as Action from '../../store/actions';

// Angular Forms
import {
	FormBuilder,
	FormGroup,
	Validators,
	FormControl,
} from '@angular/forms';

// Data Shape For This Form
interface FormValue {
	clearance: number;
	role: string;
}

@Component({
	selector: 'app-permissions-form',
	templateUrl: './permissions-form.component.html',
	styleUrls: ['./permissions-form.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PermissionsFormComponent implements OnInit {
	@Output() permissionsFormValue = new EventEmitter<
		FormValue
	>();

	currentUserPermissions$: Observable<Model.Permissions>;

	// Variables For Initial Form
	permissionsForm: FormGroup;
	roles = ['visitor', 'customer', 'employee', 'admin'];

	// Params used to build the form
	formBuilderParams = {
		clearance: [
			'',
			[Validators.required, Validators.max(4)],
		],
		role: ['', [Validators.required]],
		name: [
			'',
			[
				Validators.required,
				Validators.minLength(2),
				Validators.maxLength(6),
			],
		],
		updateOn: 'blur',
	};
	currentFormValue$: Observable<FormValue>;

	constructor(
		private fb: FormBuilder,
		private store: Store<any>,
	) {}

	ngOnInit() {
		// Build The Form
		this.permissionsForm = this.fb.group(
			this.formBuilderParams,
		);

		// Get Data From Store
		this.currentUserPermissions$ = this.store.select(
			Selector.getPermissions,
		);

		// Populate the form fields with data from the API
		this.currentUserPermissions$
			.subscribe(permissions => {
				this.permissionsForm
					.get('clearance')
					.setValue(permissions.clearance);
				this.permissionsForm
					.get('role')
					.setValue(permissions.role);
			})
			.unsubscribe();
	}

	onSubmit(form: FormGroup) {
		this.permissionsFormValue.emit(
			this.permissionsForm.value,
		);
	}

	// prettier-ignore
	getErrorMessage (formControl): string {
		return formControl.hasError('required') ? 'required' :
				formControl.hasError('max') ? 'can\'t exceed {number}' :
				formControl.hasError('min') ? 'must exceed {number}' :
				formControl.hasError('minlength') ? 'must exceed {number} characters' :
				formControl.hasError('maxlength') ? 'can\'t exceed {number} characters' :
				formControl.hasError('email') ? 'invalid email' :
				formControl.hasError('pattern') ? '' : ''
	}
}
