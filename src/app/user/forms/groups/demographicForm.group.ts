import * as Field from '../fields';
import * as FormModel from '@rbcForms/models';
import * as Model from '@user/models';

export class DemographicForm {
    age = Field.ageField;
    race = Field.raceField;
    gender = Field.genderField;

    config: FormModel.FieldConfig[];

    constructor(user?) {
        this.age.value = user ? user.age : '';
        this.race.value = user ? user.race : '';
        this.gender.value = user ? user.gender : '';

        this.config = [
            this.age,
            this.race,
            this.gender,        ];
    }

    updateValues(user) {
        this.age.value = user ? user.age : null;
        this.race.value = user ? user.race : '';
        this.gender.value = user ? user.gender : '';
    }

}
