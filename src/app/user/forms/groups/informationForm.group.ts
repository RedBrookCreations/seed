import * as Field from '../fields';
import * as FormModel from '@rbcForms/models';
import * as Model from '@user/models';

export class InformationForm {
    name = Field.nameField;
    description = Field.descriptionField;

    config: FormModel.FieldConfig[];

    constructor(user?: Model.UserData) {
        this.name.value = user ? user.name : '';
        this.description.value = user ? user.description : '';

        this.config = [
            this.name,
            this.description,
        ];
    }

    updateValues(user) {
        this.name.value = user ? user.name : '';
        this.description.value = user ? user.description : '';
    }
}
