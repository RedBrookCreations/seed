import * as Field from '../fields';
import * as FormModel from '@rbcForms/models';
import * as Model from '@user/models';

export class PermissionsForm {
    clearance = Field.clearanceField;
    role = Field.roleField;

    config: FormModel.FieldConfig[];

    constructor(permissions?: Model.Permissions) {
        this.clearance.value = permissions ? permissions.clearance : 1;
        this.role.value = permissions ? permissions.role : 'customer';

        this.config = [
            this.clearance,
            this.role,
        ];
    }

    updateValues(permissions) {
        this.clearance.value = permissions.clearance ? permissions.clearance : '1';
        this.role.value = permissions.role ? permissions.role : 'customer';
    }
}

