import {
	Component,
	OnInit,
	Output,
	EventEmitter,
	ChangeDetectionStrategy,
} from '@angular/core';

// RxJS
import { Observable } from 'rxjs';

// Models
import * as Model from '../../models';

// Angular Forms
import {
	FormBuilder,
	FormGroup,
	Validators,
	FormControl,
} from '@angular/forms';

// Data Shape For This Form
interface FormValue {
	email: string;
	password: string;
}

@Component({
	selector: 'app-login-form',
	templateUrl: './login-form.component.html',
	styleUrls: ['./login-form.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginFormComponent implements OnInit {
	@Output() loginFormValue = new EventEmitter<
		FormValue
	>();

	// VARIABLES
	loginForm: FormGroup; // Angular Form Object
	passwordShowing = false;

	// Params used to build the form
	formBuilderParams = {
		email: [
			'',
			[Validators.required, Validators.email],
		],
		password: [
			'',
			[
				Validators.required,
				Validators.minLength(6),
				Validators.pattern(
					'^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[a-z]).{6,}$',
				),
			],
		],
		updateOn: 'blur',
	};

	constructor(private fb: FormBuilder) {}

	ngOnInit() {
		// Build The Form
		this.loginForm = this.fb.group(
			this.formBuilderParams,
		);
	}

	onSubmit(form: FormGroup) {
		this.loginFormValue.emit(this.loginForm.value);
	}

	// prettier-ignore
	getErrorMessage (formControl): string {
		return formControl.hasError('required') ? 'required' :
				formControl.hasError('max') ? 'can\'t exceed {number}' :
				formControl.hasError('min') ? 'must exceed {number}' :
				formControl.hasError('minlength') ? 'at least 6 characters' :
				formControl.hasError('maxlength') ? 'can\'t exceed {number} characters' :
				formControl.hasError('email') ? 'invalid email' :
				formControl.hasError('pattern') ? '' : ''
	}
}
