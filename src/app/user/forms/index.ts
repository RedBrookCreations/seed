import { ForgotPasswordFormComponent } from './forgotPassword-form/forgotPassword-form.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { InformationFormComponent } from './information-form/information-form.component';
import { DemographicFormComponent } from './demographicForm/demographic-form.component';
export * from './fields';
export * from './groups';
import { PermissionsFormComponent } from './permissions-form/permissions-form.component';

export const forms = [
	PermissionsFormComponent,
	DemographicFormComponent,
	InformationFormComponent,
	LoginFormComponent,
	RegisterFormComponent,
ForgotPasswordFormComponent,
];
