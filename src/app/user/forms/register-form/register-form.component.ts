import {
	Component,
	OnInit,
	Output,
	EventEmitter,
	ChangeDetectionStrategy,
} from '@angular/core';

// RxJS
import { Observable } from 'rxjs';

// Models
import * as Model from '../../models';

// Store
import { Store } from '@ngrx/store';
import * as Selector from '../../store/selectors';
import * as Action from '../../store/actions';

// Angular Forms
import {
	FormBuilder,
	FormGroup,
	Validators,
	FormControl,
} from '@angular/forms';

// Data Shape For This Form
interface FormValue {
	input: number;
	select: string;
}

@Component({
	selector: 'app-register-form',
	templateUrl: './register-form.component.html',
	styleUrls: ['./register-form.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegisterFormComponent implements OnInit {
	@Output() registerFormValue = new EventEmitter<
		FormValue
	>();

	// VARIABLES
	state$: Observable<any>; // object from store
	registerForm: FormGroup; // Angular Form Object
	selectValues = [
		'selectValue1',
		'selectValue2',
		'selectValue3',
	]; // Values in Select Field
	currentFormValue$: Observable<FormValue>; // Form Value as an Observable Stream

	// Params used to build the form
	formBuilderParams = {
		select: ['', [Validators.required]],
		input: [
			'',
			[
				Validators.required,
				Validators.minLength(2),
				Validators.maxLength(6),
			],
		],
		updateOn: 'blur',
	};

	constructor(
		private fb: FormBuilder,
		private store: Store<any>,
	) {}

	ngOnInit() {
		// Build The Form
		this.registerForm = this.fb.group(
			this.formBuilderParams,
		);

		// Get Data From Store
		this.state$ = this.store
			.select
			// Selector.getState,
			();

		// Populate the form fields with data from the API
		this.state$
			.subscribe(state => {
				this.registerForm;
				// .get('')
				// .setValue(register.registerInput);
				this.registerForm;
				// .get('registerSelect')
				// .setValue(register.registerSelect);
			})
			.unsubscribe();
	}

	onSubmit(form: FormGroup) {
		this.registerFormValue.emit(
			this.registerForm.value,
		);
	}

	// prettier-ignore
	getErrorMessage (formControl): string {
		return formControl.hasError('required') ? 'required' :
				formControl.hasError('pattern') ? '' :
				formControl.hasError('max') ? 'can\'t exceed {number}' :
				formControl.hasError('min') ? 'must exceed {number}' :
				formControl.hasError('minlength') ? 'must exceed {number} characters' :
				formControl.hasError('maxlength') ? 'can\'t exceed {number} characters' :
				formControl.hasError('email') ? 'invalid email' : ''
	}
}
