import {
	Component,
	OnInit,
	Output,
	EventEmitter,
	ChangeDetectionStrategy,
} from '@angular/core';

// RxJS
import { Observable } from 'rxjs';

// Models
import * as Model from '../../models';

// Store
import { Store } from '@ngrx/store';
import * as Selector from '../../store/selectors';
import * as Action from '../../store/actions';

// Angular Forms
import {
	FormBuilder,
	FormGroup,
	Validators,
	FormControl,
} from '@angular/forms';

// Data Shape For This Form
interface FormValue {
	demographicInput: number;
	demographicSelect: string;
}

@Component({
	selector: 'app-demographic-form',
	templateUrl: './demographic-form.component.html',
	styleUrls: ['./demographic-form.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DemographicFormComponent implements OnInit {
	@Output() demographicFormValue = new EventEmitter<
		FormValue
	>();

	// VARIABLES
	userState$: Observable<Model.UserData>; // object from store
	demographicForm: FormGroup; // Angular Form Object
	raceList = [
		'White',
		'African American',
		'Latino',
		'Other',
	];
	genderList = ['Male', 'Female', 'Other'];
	currentFormValue$: Observable<FormValue>; // Form Value as an Observable Stream

	// Params used to build the form
	formBuilderParams = {
		race: ['', []],
		age: [
			'',
			[
				Validators.maxLength(3),
				Validators.max(150),
				Validators.pattern(/^\d+$/),
			],
		],
		gender: [''],
		updateOn: 'blur',
	};

	constructor(
		private fb: FormBuilder,
		private store: Store<any>,
	) {}

	ngOnInit() {
		// Build The Form
		this.demographicForm = this.fb.group(
			this.formBuilderParams,
		);

		// Get Data From Store
		this.userState$ = this.store.select(
			Selector.getCurrentUser,
		);

		// Populate the form fields with data from the API
		this.userState$
			.subscribe(user => {
				this.demographicForm
					.get('race')
					.setValue(user.race);
				this.demographicForm
					.get('age')
					.setValue(user.age);
				this.demographicForm
					.get('gender')
					.setValue(user.gender);
			})
			.unsubscribe();
	}

	onSubmit(form: FormGroup) {
		this.demographicFormValue.emit(
			this.demographicForm.value,
		);
	}

	// prettier-ignore
	getErrorMessage (formControl): string {
		return formControl.hasError('required') ? 'required' :
				formControl.hasError('pattern') ? 'Age must be a number' :
				formControl.hasError('max') ? 'You must be under 150 years old' :
				formControl.hasError('min') ? 'must exceed {number}' :
				formControl.hasError('maxlength') ? 'You are not over 1000 years old' :
				formControl.hasError('email') ? 'invalid email' : ''
	}
}
