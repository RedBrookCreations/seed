import { FieldConfig } from '../../../forms/models';

export const emailNotificationsField: FieldConfig = {
    value: false,
    formControlName: 'emailEnabled',
    componentType: 'checkbox',
    label: 'Receive Email Notifications',
};

export const pushNotificationsField: FieldConfig = {
    value: false,
    formControlName: 'pushEnabled',
    componentType: 'checkbox',
    label: 'Receive Push Notifications',
};

export const textNotificationsField: FieldConfig = {
    value: false,
    formControlName: 'textEnabled',
    componentType: 'checkbox',
    label: 'Receive Text Notifications',
};
