import { FieldConfig } from '../../../forms/models';

 export const clearanceField: FieldConfig = {
    value: '',
    formControlName: 'clearance',
    componentType: 'input',
    label: 'Clearance',
    inputConfig: {
        type: 'number',
    },
    displayConfig: {
        size: '30%'
    },
};

export const keysField: FieldConfig = {
    value: '',
    formControlName: 'keys',
    componentType: 'select',
    label: 'Keys',
    displayConfig: {
        size: '30%'
    },
};

export const roleField: FieldConfig = {
    value: '',
    formControlName: 'role',
    componentType: 'input',
    label: 'Role',
    inputConfig: {
        type: 'text',
    },
    displayConfig: {
        size: '30%'
    },
};
