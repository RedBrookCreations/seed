import { FieldConfig } from '@rbcForms/models'

export const usernameField: FieldConfig = {
	value: '',
	formControlName: 'username',
	componentType: 'input',
	label: 'Username',
	inputConfig: {
		type: 'text',
		required: true,
	},
}
