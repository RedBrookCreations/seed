import { FieldConfig } from '../../../forms/models'
import { Validators } from '@angular/forms'
import * as custValidator from './validators'

export const firstNameField: FieldConfig = {
	displayConfig: {
		size: '47%',
		marginRight: '4%',
		marginLeft: '0%',
	},
	value: '',
	formControlName: 'firstName',
	componentType: 'input',
	label: 'first name',
	inputConfig: {
		type: 'text',
	},
}

export const nameField: FieldConfig = {
	formControlName: 'name',
	componentType: 'input',
	value: '',
	label: 'Full Name',
	inputConfig: {
		type: 'text',
	},
	validators: [Validators.required],
	errors: [
		{
			validatorProperty: 'required',
			message: 'your name is required',
		},
	],
}

export const lastNameField: FieldConfig = {
	displayConfig: {
		size: '47%',
		marginRight: '0%',
	},
	value: '',
	formControlName: 'lastName',
	componentType: 'input',
	label: 'last name',
	inputConfig: {
		type: 'text',
	},
}
