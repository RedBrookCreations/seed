import { FieldConfig } from '@rbcForms/models';

export const genderField: FieldConfig = {
	value: '',
	formControlName: 'gender',
	componentType: 'select',
	selectOptions: ['Male', 'Female', 'Other'],
	label: 'gender',
	displayConfig: {
		size: '30%',
	},
	inputConfig: {
		required: false,
	},
};

export const ageField: FieldConfig = {
	value: '',
	formControlName: 'age',
	componentType: 'input',
	label: 'age',
	displayConfig: {
		size: '30%',
	},
	inputConfig: {
		type: 'number',
		required: false,
	},
};

export const raceField: FieldConfig = {
	value: '',
	formControlName: 'race',
	componentType: 'select',
	selectOptions: [
		'White',
		'African American',
		'Latino',
		'Other',
	],
	label: 'race',
	displayConfig: {
		size: '30%',
	},
	inputConfig: {
		required: false,
	},
};
