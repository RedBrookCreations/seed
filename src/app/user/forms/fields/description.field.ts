import { FieldConfig } from '@rbcForms/models';

export const descriptionField: FieldConfig = {
    displayConfig: {
        appearance: 'outline',
        size: '100%'
    },
    componentType: 'textarea',
    formControlName: 'description',
    label: 'Write your Bio Here',
    inputConfig: {
        type: 'text',
        required: true,
    }
};
