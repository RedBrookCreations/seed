import {
	Component,
	OnInit,
	Output,
	EventEmitter,
	ChangeDetectionStrategy,
} from '@angular/core';

// RxJS
import { Observable } from 'rxjs';

// Models
import * as Model from '../../models';

// Store
import { Store } from '@ngrx/store';
import * as Selector from '../../store/selectors';
import * as Action from '../../store/actions';

// Angular Forms
import {
	FormBuilder,
	FormGroup,
	Validators,
	FormControl,
} from '@angular/forms';

// Data Shape For This Form
interface FormValue {
	name: string;
	description: string;
}

@Component({
	selector: 'app-information-form',
	templateUrl: './information-form.component.html',
	styleUrls: ['./information-form.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InformationFormComponent implements OnInit {
	@Output() informationFormValue = new EventEmitter<
		FormValue
	>();

	// VARIABLES
	userState$: Observable<Model.UserData>; // object from store
	informationForm: FormGroup; // Angular Form Object

	// Params used to build the form
	formBuilderParams = {
		name: ['', [Validators.pattern(/^\w+\s+\w+/)]],
		description: ['', [Validators.maxLength(120)]],
	};

	constructor(
		private fb: FormBuilder,
		private store: Store<any>,
	) {}

	ngOnInit() {
		// Build The Form
		this.informationForm = this.fb.group(
			this.formBuilderParams,
		);

		// Get Data From Store
		this.userState$ = this.store.select(
			Selector.getCurrentUser,
		);

		// Populate the form fields with data from the API
		this.userState$
			.subscribe(user => {
				this.informationForm
					.get('name')
					.setValue(user.name);
				this.informationForm
					.get('description')
					.setValue(user.description);
			})
			.unsubscribe();
	}

	onSubmit(form: FormGroup) {
		this.informationFormValue.emit(
			this.informationForm.value,
		);
	}

	// prettier-ignore
	getErrorMessage (formControl): string {
		return formControl.hasError('required') ? 'required' :
				formControl.hasError('max') ? 'can\'t exceed {number}' :
				formControl.hasError('min') ? 'must exceed {number}' :
				formControl.hasError('minlength') ? 'must exceed {number} characters' :
				formControl.hasError('maxlength') ? 'Exceeded 120 characters' :
				formControl.hasError('email') ? 'invalid email' :
				formControl.hasError('pattern') ? 'Must have a first and last name' : ''
	}
}
