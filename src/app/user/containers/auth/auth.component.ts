import { Component, OnInit } from '@angular/core';

/* NGRX & RXJS */
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap, first } from 'rxjs/operators';

/* My Imports */
import * as Model from '@user/models';
import * as Action from '@user/store/actions';
import * as Selector from '@user/store/selectors';
import * as CoreAction from '@core/store/actions';
import * as CoreSelector from '@core/store/selectors';
import * as CoreStore from '@core/store';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
	selector: 'app-auth',
	templateUrl: './auth.component.html',
	styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
	view$: Observable<string>;
	permissions$: Observable<Model.Permissions>;
	emailSent: boolean;
	currentUser$: Observable<Model.UserData>;

	constructor(
		private store: Store<any>,
		private af: AngularFireAuth,
		private db: AngularFirestore,
	) {}

	ngOnInit() {
		this.permissions$ = this.store.select(
			Selector.getPermissions,
		);
		this.view$ = this.store.select(
			CoreSelector.getPageView,
		);
		this.currentUser$ = this.store.select(
			Selector.getCurrentUser,
		);
	}

	register(data) {
		this.af.auth
			.createUserWithEmailAndPassword(
				data.auth.email,
				data.auth.password,
			)
			.then(user => {
				const userData = this.createNewUser(
					user.user.uid,
					user.user.email,
					data.auth.name,
				);
				this.store.dispatch(
					new Action.SetUserID(user.user.uid),
				);
				this.store.dispatch(
					new Action.CreateUser(userData),
				);
			})
			.catch(err => {
				this.af.auth.signOut();
				this.store.dispatch(
					new CoreStore.ShowSnackbar(err.message),
				);
			});
	}

	emailLogin(authData) {
		this.af.auth
			.signInWithEmailAndPassword(
				authData.email,
				authData.password,
			)
			.then(user => {
				const uid = user.user.uid;
				this.store.dispatch(
					new Action.SetUserID(uid),
				);
				this.store.dispatch(
					new Action.GetUser(uid),
				);
			})
			.catch(err =>
				this.store.dispatch(
					new CoreStore.ShowSnackbar(err.message),
				),
			);
	}

	docExists(path: string) {
		return this.db
			.doc(path)
			.valueChanges()
			.pipe(first());
	}

	createUserIfUserDoesntExist(user) {
		this.docExists(`user/${user.id}`).pipe(
			tap((user: any) => {
				if (!user) {
					const userData = this.createNewUser(
						user.id,
						user.email,
						user.displayName,
					);
					this.store.dispatch(
						new Action.CreateUser(userData),
					);
				} else {
					this.store.dispatch(
						new Action.SetUserID(user.id),
					);
					this.store.dispatch(
						new Action.GetUser(user.id),
					);
				}
			}),
		);
	}

	googleoAuthLogin() {
		const provider = new auth.GoogleAuthProvider();
		return this.af.auth
			.signInWithPopup(provider)
			.then(user =>
				this.createUserIfUserDoesntExist(user.user),
			)
			.catch(
				error => new CoreAction.ShowSnackbar(error),
			);
	}

	facebookoAuthLogin() {
		const provider = new auth.FacebookAuthProvider();
		return this.af.auth
			.signInWithPopup(provider)
			.then(user =>
				this.createUserIfUserDoesntExist(user.user),
			)
			.catch(
				error => new CoreAction.ShowSnackbar(error),
			);
	}

	anonymousLogin(authData) {
		return this.af.auth
			.signInAnonymously()
			.then(user => {
				const userData = this.createNewUser(
					user.user.uid,
					user.user.email,
					'anonymous user',
				);
				this.store.dispatch(
					new Action.SetUserID(user.user.uid),
				);
				this.store.dispatch(
					new Action.CreateUser(userData),
				);
			})
			.catch(
				error => new CoreAction.ShowSnackbar(error),
			);
	}

	resetForgottenPassword(authData) {
		this.af.auth.sendPasswordResetEmail(authData.email);
		this.store.dispatch(
			new CoreStore.ShowSnackbar(
				'An email has been sent with a password reset link',
			),
		);
		this.emailSent = true;
	}

	createNewUser(id, email, name): Model.UserData {
		return {
			// Identity
			id: id,
			email: email,
			phone: '',

			// permissions
			role: 'customer',
			clearance: 1,
			keys: ['customer'],

			// Information
			description: '',
			profilePhotoUrl: '',
			coverPhotoUrl: '',
			title: '',
			name: name,
			gender: '',
			race: '',
			age: null,
		};
	}
}
