export * from './auth/auth.component';
export * from './user/user.component';
export * from './users/users.component';

import { AuthComponent } from './auth/auth.component';
import { UserComponent } from './user/user.component';
import { UsersComponent } from './users/users.component';

export const containers = [
    AuthComponent,
    UserComponent,
    UsersComponent
];
