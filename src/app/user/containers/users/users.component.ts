import { Component, OnInit } from '@angular/core';

/* NGRX & RXJS */
import { Store } from '@ngrx/store';
import { Observable, combineLatest, Subject } from 'rxjs';
import {
    map,
    tap,
    delay,
    startWith,
    withLatestFrom,
} from 'rxjs/operators';

/* My Imports */
import * as Model from '@user/models';

import * as Action from '@user/store/actions';
import * as CoreAction from '@core/store/actions';

import * as Selector from '@user/store/selectors';
import * as CoreSelector from '@core/store/selectors';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';

interface Filters {
  male: boolean;
  female: boolean;
  race: string;
  role: string;
  gender: string;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  // These are the Observables that We Will Use
  filters$: Observable<Filters>;
  loading$: Subject<boolean>;
  users$: Observable<Model.UserData[]>;

  // This is our Final Data List
  filteredUsers$: Observable<Model.UserData[]>;


  // Define the Form, The Form Values, and The Filters
  userFilterForm: FormGroup;
  genders = ['All', 'Male', 'Female', 'Other'];
  roles = ['All', 'customer', 'employee', 'admin'];
  raceFilteringOptions = ['All', 'White', 'African American', 'Other'];

  defaultFilters = {
    gender: '',
    role: '',
    race: ''
  };


  // The Constructor Allows us to Use Wizardry
  // We use this Wizardry to create A Form and to Make a Call to firebase to grab an array of users
  constructor(
    private fb: FormBuilder,
    private db: AngularFirestore,
    private store: Store<any>,
  ) {
  }

  ngOnInit() {
    this.store.dispatch(new Action.GetUsers());

    // Create the Form
    this.userFilterForm = this.fb.group(this.defaultFilters);

    // STEP 1:  Create an Observable of Both Users and Filters
    this.users$ = this.store.select(Selector.getArrayOfUsers);
    this.filters$ = this.userFilterForm.valueChanges.pipe(startWith(this.defaultFilters));
    this.loading$ = new Subject();

    // STEP 2:  Combine The Latest Emission of Both ObservablesTogether
    this.filteredUsers$ = combineLatest(this.users$, this.filters$).pipe(
      tap(() => this.loading$.next(true)),
      map(([users, filters]) => {
        // STEP 3:  Using the Current Information from both Filters and Users, Filter The Users Array by The Filters
        return users
          .filter(user => (filters.gender !== 'All' && !!filters.gender) ? user.gender === filters.gender : user)
          .filter(user => (filters.race !== 'All' && !!filters.race) ? user.race === filters.race : user)
          .filter(user => (filters.role !== 'All' && !!filters.role) ? user.role === filters.role : user);
      }),
      delay(500),
      tap(() => this.loading$.next(false))
    );
  }

  routeToUser(user) {
      this.store.dispatch(new CoreAction.Go({
          path: [`user/account/${user.id}`]
      }));
  }

  // onBack() {
  //   this.store.dispatch(new CoreAction.Back());
  // }
}

