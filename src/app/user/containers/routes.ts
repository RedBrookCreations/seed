import { Routes } from '@angular/router';
import * as Container from './index';

export const ROUTES: Routes = [
    // Auth Containers
    {
        path: 'login',
        component: Container.AuthComponent,
        data: {
            view: 'LOGIN'
        }
    },
    {
        path: 'register',
        component: Container.AuthComponent,
        data: {
            view: 'REGISTER'
        }
    },
    {
        path: 'advanced-register',
        component: Container.AuthComponent,
        data: {
            view: 'ADVANCED_REGISTER'
        }
    },
    {
        path: 'forgot',
        component: Container.AuthComponent,
        data: {
            view: 'FORGOT'
        }
    },
    // User Containers
    {
        path: 'account',
        component: Container.UserComponent,
        data: {
            view: 'READ CURRENT'
        }
    },
    {
        path: 'account/:id',
        component: Container.UserComponent,
        data: {
            view: 'READ SELECTED'
        }
    },
    {
        path: 'all',
        component: Container.UsersComponent
    },
    {
        path: 'create',
        component: Container.UserComponent,
        data: {
            view: 'CREATE'
        }
    },
];
