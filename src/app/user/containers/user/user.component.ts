import { Component, OnInit } from '@angular/core';

/* NGRX & RXJS */
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

/* My Imports */
import * as Model from '@user/models';

import * as Action from '@user/store/actions';
import * as CoreAction from '@core/store/actions';

import * as Selector from '@user/store/selectors';
import * as CoreSelector from '@core/store/selectors';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  view$: Observable<string>;
  currentUser$: Observable<Model.UserData>;
  currentId$: Observable<string>;
  selectedUser$: Observable<Model.UserData>;
  permissions$: Observable<Model.Permissions>;

  constructor(
    private store: Store<any>,
    private af: AngularFireAuth
  ) { }

  ngOnInit() {
    this.view$ = this.store.select(CoreSelector.getPageView);
    this.currentUser$ = this.store.select(Selector.getCurrentUser);
    this.selectedUser$ = this.store.select(Selector.getSelectedUserFromRouteParams);
    this.permissions$ = this.store.select(Selector.getPermissions);
    this.currentId$ = this.store.select(Selector.getCurrentUserId);

    this.store.dispatch(new CoreAction.SetCurrentFeature('User'));
  }

  createUser(data: any) {
    this.af.auth.createUserAndRetrieveDataWithEmailAndPassword(data.email, data.password)
        .then((userData) => {
            const user: Model.UserData = {
                id: userData.user.uid,
                ...data
            };
            console.log(user);
            this.store.dispatch(new Action.SetUserID(user.id));
            this.store.dispatch(new Action.CreateUser(user));
        })
        .catch((err) => this.store.dispatch(new CoreAction.ShowSnackbar(err.message)));
  }

  updateUser(event: Model.Permissions | Model.UserDemographic | Model.UserInformation | Model.UserNotificationsEnabled) {
    this.store.dispatch(new Action.UpdateUser(event));
  }

  onBack() {
    this.store.dispatch(new CoreAction.Back());
  }
}

