import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

/* NGRX */
import { Store, select } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';

/* RXJS */
import {
	switchMap,
	catchError,
	map,
	pluck,
	tap,
	concatMap,
	withLatestFrom,
} from 'rxjs/operators';
import { from, of, forkJoin } from 'rxjs';

/* Store */
import * as CoreAction from '@core/store/actions';
import { getCurrentUser } from '../selectors/user.selector';
import * as Action from '../actions';
import * as Model from '../../models';

@Injectable()
export class UserEffects {
	constructor(
		private actions$: Actions,
		private db: AngularFirestore,
		private store: Store<any>,
	) {}

	@Effect()
	getSingle$ = this.actions$.pipe(
		ofType(Action.GET_USER),
		pluck('payload'),
		switchMap((uid: string) =>
			this.db
				.collection('users')
				.doc(uid)
				.valueChanges(),
		),
		concatMap((user: Model.UserData) => [
			new Action.AddUserEntity({
				user: { id: user.id, data: { ...user } },
			}),
			new CoreAction.Go({ path: [`/user/account`] }),
		]),
		catchError(err =>
			forkJoin(() => [
				// Log the Error
				console.log(err),
				// Give user feedback on the error
				new CoreAction.ShowSnackbar(err.message),
			]),
		),
	);

	@Effect()
	create$ = this.actions$.pipe(
		ofType(Action.CREATE_USER),
		map(
			(action: Action.CreateUser): Model.UserData =>
				action.payload,
		),
		tap(payload => {
			this.db
				.collection('users')
				.doc(payload.id)
				.set(payload);
		}),
		concatMap(user => [
			new Action.AddUserEntity({
				user: { id: user.id, data: { ...user } },
			}),
			new CoreAction.ShowSnackbar(
				`An Account has been created for ${user.name}`,
			),
			new CoreAction.Go({ path: [`/user/account`] }),
		]),
		catchError(err =>
			forkJoin(() => [
				// Log the Error
				console.log(err),
				// Give user feedback on the error
				new CoreAction.ShowSnackbar(err.message),
			]),
		),
	);

	@Effect()
	getList$ = this.actions$.pipe(
		ofType(Action.GET_USERS),
		pluck('payload'),
		// Get a set of users with the role given in the payload
		switchMap(() =>
			this.db
				.collection<Model.UserData>('users')
				.valueChanges(),
		),
		// Transform the set of users into a set of Entities
		map((userDataArray: Model.UserData[]) =>
			userDataArray.map(user => {
				return { id: user.id, data: { ...user } };
			}),
		),
		// Add set of Entities to the user state
		map(
			(userEntityArray: Model.UserEntity[]) =>
				new Action.AddUserEntities({
					users: userEntityArray,
				}),
		),
		catchError(err => of(console.log(err))),
	);

	@Effect()
	update$ = this.actions$.pipe(
		ofType(Action.UPDATE_USER),
		pluck('payload'),
		// Update the Firestore Database
		tap((payload: any) =>
			this.db
				.collection('users')
				.doc(payload.id)
				.update(payload.data),
		),
		// Grab Data from Firestore Database
		switchMap((payload: Model.UserData) =>
			this.db
				.collection('users')
				.doc(payload.id)
				.valueChanges(),
		),
		concatMap((user: Model.UserData) => [
			// Set the Application state in the reducer, payload is in an Entity format
			new Action.UpsertUserEntity({
				user: { id: user.id, data: { ...user } },
			}),
			// Give the User Feedback assuming the db set was successful
			new CoreAction.ShowSnackbar(
				'Update Successful',
			),
		]),
		catchError(err =>
			forkJoin(() => [
				// Log the Error
				console.log(err),
				// Give user feedback on the error
				new CoreAction.ShowSnackbar(err.message),
			]),
		),
	);

	@Effect()
	delete$ = this.actions$.pipe(
		ofType(Action.UserActionTypes.DeleteUserEntity),
		pluck('payload'),
		tap((id: string) =>
			this.db
				.collection('users')
				.doc<Model.UserData>(id)
				.delete(),
		),
		map(id => new Action.DeleteUserEntity({ id: id })),
	);

	@Effect()
	updatePermissions$ = this.actions$.pipe(
		ofType(
			Action.UPDATE_CLEARANCE,
			Action.UPDATE_GROUPS,
			Action.UPDATE_ROLE,
			Action.UPDATE_PERMISSIONS,
		),
		pluck('payload'),
		withLatestFrom(
			this.store.pipe(select(getCurrentUser)),
		),
		// tap(([payload, user]) => this.db
		//     .collection('users')
		//     .doc(user.id)
		//     .update(payload)
		// ),
		map(([payload, user]) => {
			const updatedUser = {
				...user,
				...payload,
			};
			return new Action.UpsertUserEntity({
				user: { id: user.id, data: updatedUser },
			});
		}),
		catchError(err => of(console.log(err))),
	);

	@Effect()
	logout$ = this.actions$.pipe(
		ofType(Action.LOGOUT_USER),
		concatMap(() => [
			new Action.ClearUserEntities(),
			new CoreAction.Go({ path: ['/home'] }),
			new Action.UpdateRole({ role: 'visitor' }),
			new Action.UpdateClearance({ clearance: 0 }),
			new Action.UpdateGroups({ groups: [] }),
		]),
		catchError(err => of(console.log(err))),
	);
}
