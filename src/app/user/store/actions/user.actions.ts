import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import * as Model from '@user/models';
import { UserEntity } from '@user/models';

export const GET_USER = '[User Effect] | DB Get | Get User';
export const GET_USERS = '[User Effect] | DB Get | Get Set of Users';
export const GET_EMPLOYEE_USERS = '[User Effect] | DB Get | Get Employee Users';
export const GET_CUSTOMER_USERS = '[User Effect] | DB Get | Get Customer Users';
export const CREATE_USER = '[User Effect] | DB Set | Create User';
export const UPDATE_USER = '[User Effect] | DB Update | Update User';
export const DELETE_USER = '[User Effect] | DB Remove | Delete User';
export const LOGOUT_USER = '[Auth] Delete Token and User Attributes';

// Setting Current User Id
export const SET_USER_ID = '[Auth] Set User ID';

// Permissions
export const SET_PERMISSIONS = '[Permissions] | DB | Set Permissions in State';
export const UPDATE_PERMISSIONS = '[Permissions] | STORE | Permissions in DB';
export const UPDATE_GROUPS = '[Permissions Effect] | DB | Update Groups';
export const UPDATE_CLEARANCE = '[Permissions Effect] | DB | Update Clearance';
export const UPDATE_ROLE = '[Permissions Effect] | DB | Update Role';

export class SetPermissions implements Action {
    readonly type = SET_PERMISSIONS;
    constructor(public payload: Model.Permissions) {}
}

export class UpdatePermissions implements Action {
    readonly type = UPDATE_PERMISSIONS;
    constructor(public payload: Model.Permissions) {}
}

export class UpdateGroups implements Action {
    readonly type = UPDATE_GROUPS;
    constructor(public payload: { groups: string[] }) {}
}

export class UpdateClearance implements Action {
    readonly type = UPDATE_CLEARANCE;
    constructor(public payload: { clearance: number }) {}
}

export class UpdateRole implements Action {
    readonly type = UPDATE_ROLE;
    constructor(public payload: { role: string }) {}
}

export class LogoutUser implements Action { readonly type = LOGOUT_USER; }

export class SetUserID implements Action {
  readonly type = SET_USER_ID;
  constructor(public payload: string) {}
}

export class GetUser implements Action {
  readonly type = GET_USER;
  constructor(public payload: string) {}
}

export class GetUsers implements Action {
  readonly type = GET_USERS;
  // constructor(public payload: string) {}
}

export class CreateUser implements Action {
  readonly type = CREATE_USER;
  constructor(public payload: Model.UserData) {}
}

export class UpdateUser implements Action {
  readonly type = UPDATE_USER;
  constructor(public payload: any) {}
}

export class DeleteUser implements Action {
  readonly type = DELETE_USER;
  constructor(public payload: string) {}
}


// Actions That Update The State via Reducer
export enum UserActionTypes {
  AddUserEntity = '[User Reducer] | Entity | Add User',
  UpsertUserEntity = '[User Reducer] | Entity | Upsert User',
  AddUserEntities = '[User Reducer] | Entity | Add Users',
  UpsertUserEntities = '[User Reducer] | Entity | Upsert Users',
  UpdateUserEntity = '[User Reducer] | Entity | Update User',
  UpdateUserEntities = '[User Reducer] | Entity | Update Users',
  DeleteUserEntity = '[User Reducer] | Entity | Delete User',
  DeleteUserEntities = '[User Reducer] | Entity | Delete Users',
  ClearUserEntities = '[User Reducer] | Entity | Clear Users'
}

export class AddUserEntity implements Action {
  readonly type = UserActionTypes.AddUserEntity;
  constructor(public payload: { user: UserEntity }) {}
}

export class UpdateUserEntity implements Action {
  readonly type = UserActionTypes.UpdateUserEntity;
  constructor(public payload: { user: Update<UserEntity> }) {}
}

export class UpsertUserEntity implements Action {
  readonly type = UserActionTypes.UpsertUserEntity;
  constructor(public payload: { user: UserEntity }) {}
}

export class AddUserEntities implements Action {
  readonly type = UserActionTypes.AddUserEntities;
  constructor(public payload: { users: UserEntity[] }) {}
}

export class UpsertUserEntities implements Action {
  readonly type = UserActionTypes.UpsertUserEntities;
  constructor(public payload: { users: UserEntity[] }) {}
}

export class UpdateUserEntities implements Action {
  readonly type = UserActionTypes.UpdateUserEntities;
  constructor(public payload: { users: Update<UserEntity>[] }) {}
}

export class DeleteUserEntity implements Action {
  readonly type = UserActionTypes.DeleteUserEntity;
  constructor(public payload: { id: string }) {}
}

export class DeleteUserEntities implements Action {
  readonly type = UserActionTypes.DeleteUserEntities;
  constructor(public payload: { ids: string[] }) {}
}

export class ClearUserEntities implements Action {
  readonly type = UserActionTypes.ClearUserEntities;
}

export type UserActions = AddUserEntity
 | UpdateUserEntity
 | UpsertUserEntity
 | DeleteUserEntity
 | AddUserEntities
 | UpsertUserEntities
 | UpdateUserEntities
 | DeleteUserEntities
 | ClearUserEntities
 | CreateUser
 | GetUser
 | GetUsers
 | DeleteUser
 | UpdateUser
 | SetUserID
 | SetPermissions
 | UpdatePermissions
 | UpdateRole
 | UpdateClearance
 | UpdateGroups;
