import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as Model from '@user/models';

import * as Action from '../actions/user.actions';
import { UserActions, UserActionTypes } from '../actions/user.actions';

export interface UserState extends EntityState<Model.UserEntity> {
  // additional entities state properties
  uid: string;
}

export const userAdapter: EntityAdapter<Model.UserEntity> = createEntityAdapter<Model.UserEntity>();

const initialState: UserState = userAdapter.getInitialState({
  // additional entity state properties
  uid: null,
});

export function userReducer(
  state = initialState,
  action: UserActions
): UserState {
  switch (action.type) {

    case Action.SET_USER_ID: {
      return {
        ...state,
        uid: action.payload
      };
    }

    case Action.UserActionTypes.AddUserEntity: {
      return userAdapter.addOne(action.payload.user, state);
    }

    case UserActionTypes.UpsertUserEntity: {
      return userAdapter.upsertOne(action.payload.user, state);
    }

    case UserActionTypes.AddUserEntities: {
      return userAdapter.addMany(action.payload.users, state);
    }

    case UserActionTypes.UpsertUserEntities: {
      return userAdapter.upsertMany(action.payload.users, state);
    }

    case UserActionTypes.UpdateUserEntity: {
      return userAdapter.updateOne(action.payload.user, state);
    }

    case UserActionTypes.UpdateUserEntities: {
      return userAdapter.updateMany(action.payload.users, state);
    }

    case UserActionTypes.DeleteUserEntity: {
      return userAdapter.removeOne(action.payload.id, state);
    }

    case UserActionTypes.DeleteUserEntities: {
      return userAdapter.removeMany(action.payload.ids, state);
    }

    case UserActionTypes.ClearUserEntities: {
      return userAdapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}
