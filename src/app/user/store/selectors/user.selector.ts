import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as Model from '@user/models';

import * as UserReducer from '../reducers';
import { getRouterParamId } from '@core/store/selectors/router.selector';

export const getUserState = createFeatureSelector<UserReducer.UserState>('user');


// Grabs the UID of the user who is currently logged in
export const getCurrentUserId = createSelector(
  getUserState,
  (state) => state.uid
);

// Gets an Array of all User Objects in State
export const getArrayOfUsers = createSelector(
  getUserState,
  (state): Model.UserData[] => {
    const entities = state.entities;
    return Object.keys(entities)
      .map(id => entities[id])
      .map(user => user.data);
  }
);

export const getUserEntities = createSelector(
  getUserState,
  (state) =>  state.entities
);

// Grabs the Currently Logged In User
export const getCurrentUser = createSelector(
  getCurrentUserId,
  getUserEntities,
  (uid: string, entities) => {
    return entities[uid] ? entities[uid].data : null;
  }
);

// Grabs the Permissions Data for the Current User
// If no user is logged in, gives visitor permissions data
export const getPermissions = createSelector(
  getCurrentUser,
  (user): Model.Permissions => {
    if (user) {
      const permissions = {
        clearance: user.clearance,
        groups: user.keys,
        role: user.role,
      };

      return permissions;
    } else {
      const permissions = {
        clearance: 0,
        groups: [],
        role: 'visitor'
      };

      return permissions;
    }
  }
);

// Grabs the User whose id is currently in the Route Params
export const getSelectedUserFromRouteParams = createSelector(
  getRouterParamId,
  getUserEntities,
  (id: number, entities): Model.UserData => entities[id] ? entities[id].data : null
);

