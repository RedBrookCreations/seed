import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as CoreAction from '@core/store/actions';
import * as CoreModel from '@core/models';
import * as UISelector from '@core/store/selectors/ui.selectors';
import * as UserSelector from '@user/store/selectors';

import * as UserAction from '@user/store/actions';
import * as UserModel from '@user/models';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatTabChangeEvent } from '@angular/material';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})

export class FooterComponent implements OnInit {

  @Input() permissions;
  @Input() currentFeature;
  @Input() sidebarOpen;

  currentUser$: Observable<UserModel.UserData>;
  currentFeature$: Observable<string>;
  currentUid$: Observable<string>;
  permissions$: Observable<any>;

  @Output() toggleSidebar = new EventEmitter<boolean>();

  constructor(
    private store: Store<any>,
    private af: AngularFireAuth,
  ) { }

  ngOnInit() {
    this.currentUser$ = this.store.select(UserSelector.getCurrentUser);
    this.currentUid$ = this.store.select(UserSelector.getCurrentUserId);
    this.permissions$ = this.store.select(UserSelector.getPermissions);
  }

  onLogout() {
    this.af.auth.signOut()
      .then(() => {
        this.store.dispatch(new UserAction.LogoutUser);
      })
      .catch((err) => console.log(err));
  }

  onSidebarToggle() {
    this.toggleSidebar.emit();
  }

  changeRole(role) {
      this.store.dispatch(new UserAction.UpdateRole({ role: role }));
  }

  tabFocused() {
    if (!!this.currentFeature) {
      switch (this.currentFeature) {
        case 'Home':
          return 0;
        case 'Calendar':
          return 1;
        case 'User':
          return 2;
      }
    }
  }

  onFirstHeaderClick(event: MatTabChangeEvent) {
    switch (event.index) {
      case 0:
        this.store.dispatch(new CoreAction.Go({ path: ['/home'] }));
        return;
      case 1:
        this.store.dispatch(new CoreAction.Go({ path: ['/events/calendar'] }));
        return;
      case 2:
        this.store.dispatch(new CoreAction.Go({ path: ['/user/account'] }));
        return;
      case 3:
        this.store.dispatch(new CoreAction.Go({ path: ['admin/edit'] }));
        return;
    }
  }
}
