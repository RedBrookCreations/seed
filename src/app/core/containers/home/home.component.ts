import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import * as UserStore from '@user/store';
import * as UserModel from '@user/models';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  currentUser$: Observable<UserModel.UserData>;

  constructor(
    private store: Store<any>
  ) { }

  ngOnInit() {
    this.currentUser$ = this.store.select(UserStore.getCurrentUser);
  }


}
