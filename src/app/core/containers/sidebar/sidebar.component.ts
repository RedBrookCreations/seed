import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable, from } from 'rxjs';
import { Store } from '@ngrx/store';

import * as Model from '../../models';
import * as Action from '../../store/actions';
import * as UserModel from '@user/models';
import * as UserAction from '@user/store/actions';
import * as UserSelector from '@user/store/selectors';
import * as UISelector from '../../store/selectors/ui.selectors';
import * as Animation from '../../../shared/animations';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-sidebar',
  animations: [ Animation.slideInOutAnimation ],
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input() sidebarOpen: boolean;
  @Input() permissions;

  @Output() toggleSidebar = new EventEmitter<boolean>();

  currentUser$: Observable<UserModel.UserData>;
  currentUid$: Observable<string>;
  permissions$: Observable<any>;

  constructor(
    private store: Store<any>,
    private af: AngularFireAuth
  ) { }

  ngOnInit() {
    this.currentUser$ = this.store.select(UserSelector.getCurrentUser);
    this.currentUid$ = this.store.select(UserSelector.getCurrentUserId);
    this.permissions$ = this.store.select(UserSelector.getPermissions);
  }

  onLogout() {
    this.toggleSidebar.emit(false);
    this.af.auth.signOut()
      .then(() => {
        this.store.dispatch(new UserAction.LogoutUser);
      })
      .catch((err) => console.log(err));
  }

  changeRole(role) {
    this.store.dispatch(new UserAction.UpdateRole({ role: role }));
  }

  onRoute() {
    this.toggleSidebar.emit(false);
  }
}
