import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as CoreAction from '@core/store/actions';
import * as CoreModel from '@core/models';
import * as UISelector from '@core/store/selectors/ui.selectors';
import * as UserSelector from '@user/store/selectors';

import * as UserAction from '@user/store/actions';
import * as UserModel from '@user/models';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatBottomSheet } from '@angular/material';
import { UiService } from '@shared/services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  @Input() permissions;
  @Input() sidebarOpen;

  currentUser$: Observable<UserModel.UserData>;
  currentUid$: Observable<string>;
  permissions$: Observable<any>;

  @Output() toggleSidebar = new EventEmitter<boolean>();

  constructor(
    private store: Store<any>,
    private af: AngularFireAuth,
    private bottomSheet: MatBottomSheet,
    private ui: UiService,
  ) { }

  ngOnInit() {
    this.currentUser$ = this.store.select(UserSelector.getCurrentUser);
    this.currentUid$ = this.store.select(UserSelector.getCurrentUserId);
    this.permissions$ = this.store.select(UserSelector.getPermissions);
  }

  onLogout() {
    this.af.auth.signOut()
      .then(() => {
        this.store.dispatch(new UserAction.LogoutUser);
      })
      .catch((err) => console.log(err));
  }

  onSidebarToggle() {
    this.toggleSidebar.emit();
  }

  changeRole(role) {
      this.store.dispatch(new UserAction.UpdateRole({ role: role }));
  }

  onGetStarted() {
    this.ui.openSnackbar('Coming Soon!');
  }

}
