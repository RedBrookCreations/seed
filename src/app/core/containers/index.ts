import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';

export * from './header/header.component';
export * from './home/home.component';
export * from './sidebar/sidebar.component';
export * from './footer/footer.component';

export const containers = [
	HeaderComponent,
	HomeComponent,
	SidebarComponent,
	FooterComponent,
];
