import * as RouterEffect from './router.effects';
import * as UIEffect from './ui.effects';

export const effects: any[] = [
  RouterEffect.RouterEffects,
  UIEffect.UIEffects,
];

export * from './ui.effects';
export * from './router.effects';
