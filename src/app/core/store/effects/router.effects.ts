import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Effect, Actions, ofType } from '@ngrx/effects';
import * as Action from '../actions';

import { tap, map, distinctUntilChanged, take, switchMap, concatMap,
delay, concat } from 'rxjs/operators';
import { of } from 'rxjs';
import { ROUTER_NAVIGATION } from '@ngrx/router-store';

import * as UserStore from '@user/store';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatDialog } from '@angular/material';

@Injectable()
export class RouterEffects {

  constructor(
    private actions$: Actions,
    private afAuth: AngularFireAuth,
    private router: Router,
    private location: Location,
    private dialog: MatDialog,
  ) {}

  @Effect({ dispatch: false })
  navigate$ = this.actions$.pipe(
      ofType(Action.GO),
      map((action: Action.Go) => action.payload),
      tap(({ path, query: queryParams, extras}) => {
        this.router.navigate(path, { queryParams, ...extras });
      }),
  );

  @Effect({ dispatch: false })
  navigateBack$ = this.actions$.pipe(
      ofType(Action.BACK),
      tap(() => this.location.back()),
  );

  @Effect({ dispatch: false })
  navigateForward$ = this.actions$.pipe(
      ofType(Action.FORWARD),
      tap(() => this.location.forward()),
  );

}
