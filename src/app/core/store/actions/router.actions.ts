import { Action } from '@ngrx/store';
import * as Model from '../../models';

export const BACK = '[Router] Back';
export const FORWARD = '[Router] Forward';
export const GO = '[Router] Go';

export class Back implements Action { readonly type = BACK; }
export class Forward implements Action { readonly type = FORWARD; }
export class Go implements Action {
  readonly type = GO;
  constructor( public payload: Model.RouterNavigation) {}
}
