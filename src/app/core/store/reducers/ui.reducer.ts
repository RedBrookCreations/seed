import * as Action from '../actions';
import * as Model from '../../models';

const initialState: Model.UIState = {
    isLoading: false,
    enableForward: false,
    enableBack: false,
    viewAdditionalOptions: false,
    mobileNavState: 'hidden',
    currentFeature: 'Home'
};

export function uiReducer(state = initialState, action: Action.UIActions): Model.UIState {
    switch (action.type) {
        case (Action.START_LOADING):
            return {
                ...state,
                isLoading: true
            };
        case (Action.STOP_LOADING):
            return {
                ...state,
                isLoading: false
            };
        case (Action.CAN_GO_FORWARD):
            return {
                ...state,
                enableForward: true
            };
        case (Action.CAN_GO_BACK):
            return {
                ...state,
                enableBack: true
            };
        case (Action.CANT_GO_FORWARD):
            return {
                ...state,
                enableForward: false
            };
        case (Action.CANT_GO_BACK):
            return {
                ...state,
                enableBack: false
            };
        case (Action.SHOW_ADDITIONAL_OPTIONS):
            return {
                ...state,
                viewAdditionalOptions: true
            };
        case (Action.HIDE_ADDITIONAL_OPTIONS):
            return {
                ...state,
                viewAdditionalOptions: false
            };
        case (Action.SHOW_MOBILE_NAV):
            return {
                ...state,
                mobileNavState: 'shown'
            };
        case (Action.HIDE_MOBILE_NAV):
            return {
                ...state,
                mobileNavState: 'hidden'
            };
        case (Action.SET_CURRENT_FEATURE):
            return {
                ...state,
                currentFeature: action.payload
            };
        default:
            return state;
    }
}
