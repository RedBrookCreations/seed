import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import * as Router from '@ngrx/router-store';
import * as Model from '../../models';

export class CustomSerializer implements Router.RouterStateSerializer<Model.RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): Model.RouterStateUrl {
    const { url } = routerState;
    const { queryParams } = routerState.root;
    let state: ActivatedRouteSnapshot = routerState.root;
    while (state.firstChild) {
      state = state.firstChild;
    }
    const { params } = state;
    const { component } = state;
    const { data } = state;
    return { url, component, queryParams, params, data };
  }
}




