import * as Router from '@ngrx/router-store';

import * as UserEffect from '@user/store/effects';
import * as CoreEffect from '@core/store/effects';

import * as CoreReducer from '@core/store/reducers';
import * as UserReducer from '@user/store/reducers';

import * as CoreModel from '@core/models';
import * as UserModel from '@user/models';
import * as UserStore from '@user/store';

import { createFeatureSelector, ActionReducerMap } from '@ngrx/store';

// Model
export interface CoreState {
    routerState: Router.RouterReducerState<CoreModel.RouterStateUrl>;
    ui: CoreModel.UIState;
    user: UserStore.UserState;
}

// Reducer
export const reducers: ActionReducerMap<CoreState> = {
    routerState: Router.routerReducer,
    ui: CoreReducer.uiReducer,
    user: UserStore.userReducer
};

// Effects
export const effects: any[] = [
    CoreEffect.UIEffects,
    CoreEffect.RouterEffects,
    UserStore.UserEffects
];

// Selector
export const getCoreState = createFeatureSelector<CoreState>('root');
