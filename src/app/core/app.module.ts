import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// environment
import { environment } from '../../environments/environment';

// bootstrap
import { AppComponent } from './app.component';

// NGRX
import { StoreModule, MetaReducer } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
export const metaReducers: MetaReducer<any>[] = !environment.production ? [storeFreeze] : [];

// PWA
import { ServiceWorkerModule } from '@angular/service-worker';

// routes
import { ROUTES } from '../routes';

// store
import { effects, reducers } from '@core/store/state';
import { CustomSerializer } from '@core/store';

// components
import { components } from '@core/components';

// containers
import { containers } from '@core/containers';

// shared
import { SharedModule } from '../shared/shared.module';
import { NotificationsModule } from 'app/notifications/notifications.module';

@NgModule({
  declarations: [
    AppComponent,
    ...components,
    ...containers,
  ],
  imports: [
    NotificationsModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(ROUTES),
    StoreModule.forRoot(reducers, { metaReducers}),
    EffectsModule.forRoot(effects),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    SharedModule,
    StoreRouterConnectingModule,
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  providers: [
    { provide: RouterStateSerializer, useClass: CustomSerializer },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
