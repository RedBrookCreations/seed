export interface UIState {
    isLoading: boolean;
    enableForward:  boolean;
    enableBack:  boolean;
    viewAdditionalOptions: boolean;
    mobileNavState: string;
    currentFeature: string;
}
