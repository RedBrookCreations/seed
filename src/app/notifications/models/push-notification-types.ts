export declare type PushSubscriptionResult = 'denied' | 'granted' | 'default';
export declare type PushSubscriptionDirection = 'auto' | 'ltr' | 'rtl';
