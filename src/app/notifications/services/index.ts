import { PushSubscriptionService } from './push-subscriptions.service';
import { PushNotificationsService } from './push-notification.service';
import { Provider } from '@angular/core';

export * from './push-subscriptions.service';
export * from './push-notification.service';

export const services: Provider[] = 
[
    PushSubscriptionService,
    PushNotificationsService
];