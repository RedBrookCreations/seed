import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import * as Model from '@scheduling/models';
import { CategoryActions, CategoryActionTypes } from '../actions/category.actions';

export interface CategoryState extends EntityState<Model.CategoryEntity> {
  // additional entities state properties
}

export const categoryAdapter: EntityAdapter<Model.CategoryEntity> = createEntityAdapter<Model.CategoryEntity>();

const initialState: CategoryState = categoryAdapter.getInitialState({
  // additional entity state properties
});

export function categoryReducer(
  state = initialState,
  action: CategoryActions
): CategoryState {
  switch (action.type) {

    case CategoryActionTypes.AddCategoryEntity: {
      return categoryAdapter.addOne(action.payload.category, state);
    }

    case CategoryActionTypes.UpsertCategoryEntity: {
      return categoryAdapter.upsertOne(action.payload.category, state);
    }

    case CategoryActionTypes.AddCategoryEntities: {
      return categoryAdapter.addMany(action.payload.categories, state);
    }

    case CategoryActionTypes.UpsertCategoryEntities: {
      return categoryAdapter.upsertMany(action.payload.categories, state);
    }

    case CategoryActionTypes.UpdateCategoryEntity: {
      return categoryAdapter.updateOne(action.payload.category, state);
    }

    case CategoryActionTypes.UpdateCategoryEntities: {
      return categoryAdapter.updateMany(action.payload.categories, state);
    }

    case CategoryActionTypes.DeleteCategoryEntity: {
      return categoryAdapter.removeOne(action.payload.id, state);
    }

    case CategoryActionTypes.DeleteCategoryEntities: {
      return categoryAdapter.removeMany(action.payload.ids, state);
    }

    case CategoryActionTypes.ClearCategoryEntities: {
      return categoryAdapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}
