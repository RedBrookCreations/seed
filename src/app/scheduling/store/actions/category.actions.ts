import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { CategoryEntity } from '@scheduling/models/category.model';

import * as Model from '@scheduling/models';

// Actions that Query/Update the Database via Effect
export const GET_CATEGORY = '[Category Effect] | DB Get | Get Category';
export const GET_CATEGORIES = '[Category Effect] | DB Get | Get Set of Categories';
export const GET_EMPLOYEE_CATEGORIES = '[Category Effect] | DB Get | Get Employee Categories';
export const GET_CUSTOMER_CATEGORIES = '[Category Effect] | DB Get | Get Customer Categories';
export const CREATE_CATEGORY = '[Category Effect] | DB Set | Create Category';
export const UPDATE_CATEGORY = '[Category Effect] | DB Update | Update Category';
export const DELETE_CATEGORY = '[Category Effect] | DB Remove | Delete Category';


export class GetCategory implements Action {
  readonly type = GET_CATEGORY;
  constructor(public payload: string) {}
}

export class GetCategories implements Action {
  readonly type = GET_CATEGORIES;
}

export class CreateCategory implements Action {
  readonly type = CREATE_CATEGORY;
  constructor(public payload: Model.CategoryData) {}
}

export class UpdateCategory implements Action {
  readonly type = UPDATE_CATEGORY;
  constructor(public payload: Model.CategoryData) {}
}

export class DeleteCategory implements Action {
  readonly type = DELETE_CATEGORY;
  constructor(public payload: string) {}
}


// Actions That Update The State via Reducer
export enum CategoryActionTypes {
  AddCategoryEntity = '[Category Reducer] | Entity | Add Category',
  UpsertCategoryEntity = '[Category Reducer] | Entity | Upsert Category',
  AddCategoryEntities = '[Category Reducer] | Entity | Add Categories',
  UpsertCategoryEntities = '[Category Reducer] | Entity | Upsert Categories',
  UpdateCategoryEntity = '[Category Reducer] | Entity | Update Category',
  UpdateCategoryEntities = '[Category Reducer] | Entity | Update Categories',
  DeleteCategoryEntity = '[Category Reducer] | Entity | Delete Category',
  DeleteCategoryEntities = '[Category Reducer] | Entity | Delete Categories',
  ClearCategoryEntities = '[Category Reducer] | Entity | Clear Categories'
}

export class AddCategoryEntity implements Action {
  readonly type = CategoryActionTypes.AddCategoryEntity;
  constructor(public payload: { category: CategoryEntity }) {}
}

export class UpdateCategoryEntity implements Action {
  readonly type = CategoryActionTypes.UpdateCategoryEntity;
  constructor(public payload: { category: Update<CategoryEntity> }) {}
}

export class UpsertCategoryEntity implements Action {
  readonly type = CategoryActionTypes.UpsertCategoryEntity;
  constructor(public payload: { category: CategoryEntity }) {}
}

export class AddCategoryEntities implements Action {
  readonly type = CategoryActionTypes.AddCategoryEntities;
  constructor(public payload: { categories: CategoryEntity[] }) {}
}

export class UpsertCategoryEntities implements Action {
  readonly type = CategoryActionTypes.UpsertCategoryEntities;
  constructor(public payload: { categories: CategoryEntity[] }) {}
}

export class UpdateCategoryEntities implements Action {
  readonly type = CategoryActionTypes.UpdateCategoryEntities;
  constructor(public payload: { categories: Update<CategoryEntity>[] }) {}
}

export class DeleteCategoryEntity implements Action {
  readonly type = CategoryActionTypes.DeleteCategoryEntity;
  constructor(public payload: { id: string }) {}
}

export class DeleteCategoryEntities implements Action {
  readonly type = CategoryActionTypes.DeleteCategoryEntities;
  constructor(public payload: { ids: string[] }) {}
}

export class ClearCategoryEntities implements Action {
  readonly type = CategoryActionTypes.ClearCategoryEntities;
}

export type CategoryActions = AddCategoryEntity
 | UpdateCategoryEntity
 | UpsertCategoryEntity
 | DeleteCategoryEntity
 | AddCategoryEntities
 | UpsertCategoryEntities
 | UpdateCategoryEntities
 | DeleteCategoryEntities
 | ClearCategoryEntities
 | CreateCategory
 | GetCategory
 | GetCategories
 | DeleteCategory
 | UpdateCategory;
