import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

/* NGRX */
import { Store, select } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';

/* RXJS */
import { switchMap, catchError, map, pluck, tap, concatMap, withLatestFrom } from 'rxjs/operators';
import { from, of, forkJoin } from 'rxjs';

/* Store */
import * as CoreAction from '@core/store/actions';
import * as Action from '../actions';
import * as Model from '../../models';

@Injectable()
export class EventEffects {
  constructor(
    private actions$: Actions,
    private db: AngularFirestore,
    private store: Store<any>,
  ) {}

  @Effect()
  getSingle$ = this.actions$.pipe(
    ofType(Action.GET_EVENT),
    pluck('payload'),
    switchMap((uid: string) => this.db
        .collection('events')
        .doc(uid)
        .valueChanges()
    ),
    map((events: Model.EventData) => new Action.AddEventEntity({ event: { id: events.id, data: { ...events }}})),
    catchError(err => of(console.log(err)))
  );

  @Effect()
  create$ = this.actions$.pipe(
    ofType(Action.CREATE_EVENT),
    map((action: Action.CreateEvent): Model.EventData => action.payload),
    tap((payload) => {
      this.db
        .collection('events')
        .doc(payload.id)
        .set(payload);
    }),
    map((events) => new Action.AddEventEntity({ event: { id: events.id, data: { ...events }}})),
    catchError(err => of(console.log(err)))
  );


  @Effect()
  getList$ = this.actions$.pipe(
    ofType(Action.GET_EVENTS),
    pluck('payload'),
    // Get a set of events with the role given in the payload
    switchMap(() => this.db
        .collection<Model.EventData>('events')
        .valueChanges()
    ),
    // Transform the set of events into a set of Entities
    map((eventsDataArray: Model.EventData[]) => eventsDataArray.map((events) => {
      return { id: events.id, data: { ...events } };
    })),
    // Add set of Entities to the events state
    map((eventsEntityArray: Model.EventEntity[]) => new Action.AddEventEntities( { events: eventsEntityArray })),
    catchError(err => of(console.log(err)))
  );

  @Effect()
  update$ = this.actions$.pipe(
    ofType(Action.UPDATE_EVENT),
    pluck('payload'),
    // Update the Firestore Database
    tap((payload: any) => this.db
      .collection('events')
      .doc(payload.id)
      .update(payload.data)
    ),
    // Grab Data from Firestore Database
    switchMap((payload: Model.EventData) => this.db
      .collection('events')
      .doc(payload.id)
      .valueChanges()
    ),
    concatMap((events: Model.EventData) => [
      // Set the Application state in the reducer, payload is in an Entity format
      new Action.UpsertEventEntity({ event: { id: events.id, data: { ...events }}}),
      // Give the Event Feedback assuming the db set was successful
      new CoreAction.ShowSnackbar('Update Successful'),
    ]),
    catchError((err) => forkJoin(() => [
      // Log the Error
      console.log(err),
      // Give events feedback on the error
      new CoreAction.ShowSnackbar(err.message)
    ]))
  );

  @Effect()
  delete$ = this.actions$.pipe(
    ofType(Action.EventActionTypes.DeleteEventEntity),
    pluck('payload'),
    tap((id: string) => this.db
        .collection('events')
        .doc<Model.EventData>(id)
        .delete()
    ),
    map(id => new Action.DeleteEventEntity({ id: id }))
  );

}
