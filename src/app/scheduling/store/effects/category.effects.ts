import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

/* NGRX */
import { Store, select } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';

/* RXJS */
import { switchMap, catchError, map, pluck, tap, concatMap, withLatestFrom } from 'rxjs/operators';
import { from, of, forkJoin } from 'rxjs';

/* Store */
import * as CoreAction from '@core/store/actions';
import * as Action from '../actions';
import * as Model from '../../models';

@Injectable()
export class CategoryEffects {
  constructor(
    private actions$: Actions,
    private db: AngularFirestore,
    private store: Store<any>,
  ) {}

  @Effect()
  getSingle$ = this.actions$.pipe(
    ofType(Action.GET_CATEGORY),
    pluck('payload'),
    switchMap((uid: string) => this.db
        .collection('categories')
        .doc(uid)
        .valueChanges()
    ),
    map((categories: Model.CategoryData) => new Action.AddCategoryEntity({ category: { id: categories.id, data: { ...categories }}})),
    catchError(err => of(console.log(err)))
  );

  @Effect()
  getList$ = this.actions$.pipe(
    ofType(Action.GET_CATEGORIES),
    pluck('payload'),
    // Get a set of categories with the role given in the payload
    switchMap(() => this.db
        .collection<Model.CategoryData>('categories')
        .valueChanges()
    ),
    // Transform the set of categories into a set of Entities
    map((categoriesDataArray: Model.CategoryData[]) => categoriesDataArray.map((categories) => {
      return { id: categories.id, data: { ...categories } };
    })),
    // Add set of Entities to the categories state
    map((categoriesEntityArray: Model.CategoryEntity[]) => new Action.AddCategoryEntities( { categories: categoriesEntityArray })),
    catchError(err => of(console.log(err)))
  );

  @Effect()
  create$ = this.actions$.pipe(
    ofType(Action.CREATE_CATEGORY),
    map((action: Action.CreateCategory) => action.payload),
    tap((payload: Model.CategoryData) => {
      this.db
        .collection('categories')
        .doc(payload.id)
        .set(payload);
    }),
    map((categories) => new Action.AddCategoryEntity({ category: { id: categories.id, data: { ...categories }}})),
    catchError(err => of(console.log(err)))
  );

  @Effect()
  update$ = this.actions$.pipe(
    ofType(Action.UPDATE_CATEGORY),
    pluck('payload'),
    // Update the Firestore Database
    tap((payload: any) => this.db
      .collection('categories')
      .doc(payload.id)
      .update(payload.data)
    ),
    // Grab Data from Firestore Database
    switchMap((payload: Model.CategoryData) => this.db
      .collection('categories')
      .doc(payload.id)
      .valueChanges()
    ),
    concatMap((categories: Model.CategoryData) => [
      // Set the Application state in the reducer, payload is in an Entity format
      new Action.UpsertCategoryEntity({ category: { id: categories.id, data: { ...categories }}}),
    ]),
    catchError((err) => forkJoin(() => [
      // Log the Error
      console.log(err),
      // Give categories feedback on the error
      new CoreAction.ShowSnackbar(err.message)
    ]))
  );

  @Effect()
  delete$ = this.actions$.pipe(
    ofType(Action.DELETE_CATEGORY),
    pluck('payload'),
    tap((id: string) => this.db
        .collection('categories')
        .doc<Model.CategoryData>(id)
        .delete()
    ),
    map(id => new Action.DeleteCategoryEntity({ id: id }))
  );

}
