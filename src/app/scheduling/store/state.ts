import * as schedulingEffect from '@scheduling/store/effects';

import * as SchedulingReducer from '@scheduling/store/reducers';

import { createFeatureSelector, ActionReducerMap } from '@ngrx/store';

// Model
export interface SchedulingState {
    event: SchedulingReducer.EventState;
    category: SchedulingReducer.CategoryState;
}

// Reducer
export const reducers: ActionReducerMap<SchedulingState> = {
    event: SchedulingReducer.eventReducer,
    category: SchedulingReducer.categoryReducer,
};

// Effects
export const effects: any[] = [
    schedulingEffect.EventEffects,
    schedulingEffect.CategoryEffects,
];

// Selector
export const getSchedulingState = createFeatureSelector<SchedulingState>('scheduling');

