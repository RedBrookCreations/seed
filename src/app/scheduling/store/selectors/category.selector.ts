import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as CategoryReducer from '@scheduling/store/reducers/category.reducer';
import * as Model from '@scheduling/models';

import { getSchedulingState } from '@scheduling/store/state';
import { getRouterParamId } from '@core/store/selectors';

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = CategoryReducer.categoryAdapter.getSelectors();

export const getCategoryState = createSelector(
  getSchedulingState,
  (state) => state.category
);

export const getArrayOfCategorys = createSelector(
  getCategoryState,
  (categoryState): Model.CategoryData[] => {
    const entities = categoryState.entities;
    return Object.keys(entities)
      .map(id => entities[id])
      .map(category => category.data);
  }
);

export const getCategoryEntities = createSelector(
  getCategoryState,
  (state) =>  state.entities
);

export const getSelectedCategory = createSelector(
    getCategoryState,
    (state) => state
);

export const getSelectedCategoryFromRouteParams = createSelector(
  getRouterParamId,
  getCategoryEntities,
  (id: number, entities): Model.CategoryEntity => entities[id]
);

