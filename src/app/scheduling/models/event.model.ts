import { CategoryData } from './category.model';

export interface EventEntity {
  id: string;
  data: EventData;
}

export interface EventDate {
  day: number;
  month: number;
  year: number;
  time?: string;
}

export interface EventData {
  id: string;
  date: EventDate;
  name: string;
  startTime: string;
  endTime: string;
  description: string;
  category: string;
  owner: string; // owner id;
  // associatedUserIds: string[]; /* list of uids */
  // owner: AssociatedUser[];
  // going: AssociatedUser[];
  // invited: AssociatedUser[];
  // interested: AssociatedUser[];
}

export interface AssociatedUser {
  id: string;
  name: string;
}

export interface TileConfig {
  day: number;
  month: number;
  year: number;
  events: EventData[];
  color: string;
}
