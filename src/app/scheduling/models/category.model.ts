/* NOTE:
EVENTS are filtered by the CALENDAR they are associated with
*/

export interface CategoryEffectPayload<T> {
  role?: string;
  data: T;
}

export interface CategoryEntity {
  id: string;
  data: CategoryData;
}

export interface CategoryData {
  id: string;
  name: string;
  owner: string;
  color: string;
  viewing: boolean;
}


