import { FieldConfig } from '@rbcForms/models';

export const eventCategories: FieldConfig = {
    componentType: 'select',
    formControlName: 'category',
    selectOptions: [],
    label: 'Choose Category',
    value: '',
    inputConfig: {
        required: true,
    },
    displayConfig: {
        marginLeft: '0%',
        marginRight: '0%',
    }
};
