import { FieldConfig } from '@rbcForms/models';

export const viewType: FieldConfig = {
    formControlName: 'currentView',
    componentType: 'select',
    value: 'Month',
    label: 'Current View',
    selectOptions: [
        'Month',
        'Week',
        'Day',
    ],
    displayConfig: {
        size: '100%',
        appearance: 'outline',
        marginTop: '5%',
    },
    extras: {
        help: 'choose the format to view events'
    }
};
