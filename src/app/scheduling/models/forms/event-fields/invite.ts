import { FieldConfig } from '@rbcForms/models';

export const eventAttendeesField: FieldConfig = {
    componentType: 'select',
    formControlName: 'attendees',
    selectOptions: [],
    label: 'Event Attendees',
    inputConfig: {
        required: true,
    }
};
