import { FieldConfig } from '@rbcForms/models';

export const eventColor: FieldConfig = {
    componentType: 'select',
    formControlName: 'color',
    selectOptions: [],
    label: 'Event Color',
    inputConfig: {
        required: true,
    }
};
