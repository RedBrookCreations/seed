import { FieldConfig } from '@rbcForms/models';

export const eventDescription: FieldConfig = {
    displayConfig: {
        appearance: 'outline',
        size: '100%'
    },
    componentType: 'textarea',
    formControlName: 'description',
    label: 'Event Description',
    value: '',
    inputConfig: {
        type: 'text',
        required: true,
    }
};
