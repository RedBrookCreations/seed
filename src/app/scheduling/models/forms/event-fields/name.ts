import { FieldConfig } from '@rbcForms/models';

export const eventName: FieldConfig = {
    componentType: 'title',
    formControlName: 'name',
    label: 'Event Title',
    inputConfig: {
        type: 'text',
        required: true,
    },
};
