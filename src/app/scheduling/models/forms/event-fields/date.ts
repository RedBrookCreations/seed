import { FieldConfig } from '@rbcForms/models';

export const dateField: FieldConfig = {
    formControlName: 'date',
    componentType: 'date',
    value: '',
    label: 'choose month',
    inputConfig: {
        type: 'date',
    },
    displayConfig: {
        size: '30%',
        marginLeft: '0%',
        marginRight: '0%'
    }
};
