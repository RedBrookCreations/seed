// $amber-A700: #FFAB00;
// $blue-A700: #2962FF;
// $teal-A700: #00BFA
// $red-A700: #d50000;
// $purple-A700: #AA00FF;
// $pink-A700: #C51162;
// $orange-A700: #FF6D00;
// $lime-A700: #AEEA00;
// $light-green-A700: #64DD17;
// $light-blue-A700: #0091EA;
// $indigo-A700: #304FFE;
// $green-A700: #00C853;
// $deep-purple-A700: #6200EA;
// $deep-orange-A700: #DD2C00;
// $cyan-A700: #00B8D4;

import { FieldConfig } from '@rbcForms/models';

export const colorPicker: FieldConfig = {
    componentType: 'select',
    formControlName: 'color',
    selectOptions: [
        'amber', 'blue', 'teal',
        'red', 'purple', 'pink',
        'orange', 'lime', 'lightgreen',
        'lightblue', 'indigo', 'green',
        'deeppurple', 'deeporange', 'cyan'],
    label: 'Category Color',
    value: '',
    inputConfig: {
        required: true,
    },
    displayConfig: {
        marginLeft: '0%',
        marginRight: '0%',
    }
};


