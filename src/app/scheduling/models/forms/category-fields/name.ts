import { FieldConfig } from '@rbcForms/models';

export const categoryName: FieldConfig = {
    componentType: 'input',
    formControlName: 'name',
    label: 'Category Name',
    inputConfig: {
        type: 'text',
        required: true,
    }
};
