import * as Field from '../event-fields';
import * as FormModel from '@rbcForms/models';
import * as Model from '@scheduling/models';

export class EventForm {

    name = Field.eventName;
    startDate = Field.eventStartDate;
    startTime = Field.eventStartTime;
    endDate = Field.eventEndDate;
    endTime = Field.eventEndTime;
    description = Field.eventDescription;
    category = Field.eventCategories;

    config: FormModel.FieldConfig[];

    constructor(categories?: Model.CategoryData[]) {

        this.config = [
            this.name,
            this.startDate,
            this.startTime,
            this.endTime,
            this.description,
            this.category
        ];
    }

    addCalendarOptions(categories: Model.CategoryData[]) {
        this.category.selectOptions = !!categories ? categories.map(category => category.name) : [];
    }

    updateDate(date: Model.EventDate) {
        const startDate = new Date(date.year, date.month, date.day);
        this.startDate.value = date ? startDate : '';
    }

    updateValues(event) {
        this.name.value = event ? event.name : '';
        this.description.value = event ? event.description : '';
        this.startDate.value = event ? event.startDate : '';
        this.startTime.value = event ? event.startTime : '';
        this.endTime.value = event ? event.endTime : '';
        this.category.value = event ? event.category : 'Company Events';
    }
}
