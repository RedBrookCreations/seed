import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

// RBC Forms
import { FieldConfig } from '@rbcForms/models';
import * as Field from '@scheduling/models/forms';
import * as Form from '@scheduling/models/forms/groups';

// My Imports
import * as Model from '@scheduling/models';
import * as UserModel from '@user/models';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-update-event',
  templateUrl: './update.component.html',
  styles: [`

  `]
})
export class UpdateEventComponent implements OnInit {
  @Input() event: Model.EventData;
  @Input() currentUser: UserModel.UserData;
  @Input() permissions: UserModel.Permissions;
  @Input() categories: Model.CategoryData[];

  @Output() update = new EventEmitter<any>();


  config:  FieldConfig[];
  valid = false;
  form: any;
  formData: any;

  constructor(
    private db: AngularFirestore
  ) { }

  ngOnInit() {

    this.form = new Form.EventForm();
    this.config = this.form.config;

    this.form.addCalendarOptions(this.categories);

    if (this.categories) {
      this.form.addCalendarOptions(this.categories);
    }

    if (this.event) {
      this.form.updateValues(this.event);
      this.form.updateDate(this.event.date);
    }
  }

  formChanged(formData) {
    this.form.updateValues(formData.value);
    this.formData = formData.value;
    this.valid = formData.valid;
  }

  updateEvent() {
    const eventData = {
      ...this.formData
    };

    this.update.emit(eventData);
  }
}
