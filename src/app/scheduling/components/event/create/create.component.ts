import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

// RBC Forms
import { FieldConfig } from '@rbcForms/models';
import * as Field from '@scheduling/models/forms';
import * as Form from '@scheduling/models/forms/groups';

// My Imports
import * as Model from '@scheduling/models';
import * as UserModel from '@user/models';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-create-event',
  templateUrl: './create.component.html',
  styles: [`

  `]
})
export class CreateEventComponent implements OnInit {
  @Input() event: Model.EventData;
  @Input() currentUser: UserModel.UserData;
  @Input() permissions: UserModel.Permissions;
  @Input() categories: Model.CategoryData[];

  @Output() create = new EventEmitter<any>();


  config:  FieldConfig[];
  valid = false;
  form: any;
  formData: any;

  constructor(
    private db: AngularFirestore
  ) { }

  ngOnInit() {

    this.form = new Form.EventForm();
    this.config = this.form.config;

    this.form.addCalendarOptions(this.categories);
    if (this.categories) {
      this.form.addCalendarOptions(this.categories);
    }

    if (this.event) {
      this.form.updateDate(this.event.date);
    }
  }

  formChanged(formData) {
    // console.log(formData.value);
    this.form.updateValues(formData.value);
    this.formData = formData.value;
    this.valid = formData.valid;
  }

  createEvent() {
    const year = this.formData.startDate.getFullYear();
    const month = this.formData.startDate.getMonth();
    const day = this.formData.startDate.getDate(month, year, 0);

    const eventData: Model.EventData = {
      id: this.db.createId(),
      date: {
        day: day,
        month: month,
        year: year,
      },
      name: this.formData.name,
      startTime: this.formData.startTime,
      endTime: this.formData.endTime,
      description: this.formData.description,
      category: this.formData.category,
      owner: this.currentUser.id
    };
    // console.log(eventData);
    this.create.emit(eventData);
  }
}
