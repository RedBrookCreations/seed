import {
Component,
OnInit,
Input,
Output,
EventEmitter,
ChangeDetectionStrategy,
} from '@angular/core';



// Model Imports
import * as Model from '@scheduling/models';
import * as UserModel from '@user/models';
import { AngularFireStorage } from '@angular/fire/storage';
import { MatDialog } from '@angular/material';

@Component({
    selector: 'app-read-event',
    templateUrl: './read.component.html',
    styleUrls: ['./read.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReadEventComponent implements OnInit {
    @Input() event: Model.EventData;
    @Input() categories: Model.CategoryData[];
    @Input() permissions: UserModel.Permissions;
    @Input() currentUserId: string;

    @Output() route = new EventEmitter<string>();
    @Output() update = new EventEmitter<any>();

    date;

    profilePhoto;
    coverPhoto;
    currentTab: string;
    // tabs = ['Profile', 'Posts', 'Calendar', 'Pictures', 'Videos', 'Settings'];
    tabs = ['About', 'Settings'];
    constructor(
        private dialog: MatDialog
    ) {}

    ngOnInit() {
        this.dialog.closeAll();

        this.date = new Date(this.event.date.year, this.event.date.month, this.event.date.day).toDateString();
        this.currentTab = 'Settings';
    }

    changeTab(tab) {
        this.currentTab = tab;
    }

    updateEvent(event) { this.update.emit(event); }

}


