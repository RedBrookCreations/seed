import { CreateEventComponent } from './create/create.component';
import { UpdateEventComponent } from './update/update.component';
import { ListEventComponent } from './list/list.component';
import { ReadEventComponent } from './read/read.component';

export * from './create/create.component';
export * from './update/update.component';
export * from './list/list.component';
export * from './read/read.component';

export const eventComponents = [
    CreateEventComponent,
    UpdateEventComponent,
    ListEventComponent,
    ReadEventComponent
];
