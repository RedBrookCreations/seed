import {
Component,
OnInit,
Input,
Output,
EventEmitter,
OnChanges,
ChangeDetectionStrategy,
} from '@angular/core';

// Model Imports
import * as Model from '@scheduling/models';
import * as UserModel from '@user/models';

@Component({
    selector: 'app-list-event',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListEventComponent implements OnChanges {
    @Input() events: Model.EventData[];
    @Input() currentUser: UserModel.UserData;
    @Input() permissions: UserModel.Permissions;

    @Output() eventClicked = new EventEmitter<Model.EventData>();

    // Support RBC Form
    loading = false;

    constructor() {}

    ngOnChanges() {
        this.events = this.events.filter(event => event.owner === this.currentUser.id);
    }

    onClick(event) {
        this.eventClicked.emit(event);
    }
}


