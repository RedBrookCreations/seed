
export * from './calendar';
export * from './event';

import { calendarComponents, modalComponents } from './calendar';
import { eventComponents } from './event';

// Components For This Module
export const components = [
    ...calendarComponents,
    ...modalComponents,
    ...eventComponents,
];


