import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges
} from '@angular/core';

// Support The Modal
import { MatDialog } from '@angular/material';
import { DayOfMonthModalComponent } from './day-of-month-modal.component';

import * as Model from '@scheduling/models';

export interface TileConfig {
  day: number;
  month: number;
  year: number;
  events: Model.EventData[];
  color: string;
}

@Component({
  selector: 'app-month',
  templateUrl: './month.component.html',
  styleUrls: ['./month.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MonthComponent implements OnChanges {
  @Input() events: Model.EventData[];
  @Input() categories: Model.CategoryData[];
  @Input() year: number;
  @Input() month: number;

  @Input() permissions;
  @Input() currentUser;

  @Output() chosenEvent = new EventEmitter<Event>();
  @Output() chosenDay = new EventEmitter<any>();

  tiles: TileConfig[];
  dayAbbreviations = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  mobileDayAbbreviations = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];




  constructor(
    private dialog: MatDialog,
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    this.tiles = this.generateMonthOfTiles(this.month, this.year, this.events, this.categories);
    // console.log(this.categories);
  }

  onChooseEvent(event) {
    this.chosenEvent.emit(event);
  }

  onChooseDay(dayTile: Model.TileConfig) {
    const eventFormData = {
      date: { day: dayTile.day, month: dayTile.month, year: dayTile.year},
      name: '',
      startTime: '',
      endTime: '',
      description: '',
    };
    const data = {
      events: dayTile.events,
      categories: this.categories,
      event:  eventFormData,
      currentUser: this.currentUser,
      permissions: this.permissions,
    };
    this.chosenDay.emit(data);
  }

  chooseDay(tileData: Model.TileConfig) {
    const dialogRef = this.dialog.open(DayOfMonthModalComponent, {
      data: {
        day: { ...tileData },
        currentUser: this.currentUser,
        permissions: this.permissions
     }
    });
  }

  generateMonthOfTiles(month: number, year: number, events: Model.EventData[], categories: Model.CategoryData[]) {
    const tiles = [];
    const daysInCurrentMonth = new Date(year, month + 1, 0).getDate();
    const daysInPreviousMonth = new Date(year, month, 0).getDate();
    const firstDayOfCurrentMonth = new Date(`${year}-${month + 1}-01`).getDay();

    // Add days from previous calendar on the first row until the first day of the current month is reached
    if (firstDayOfCurrentMonth !== 6) {
        let dayFromPreviousMonth = daysInPreviousMonth - firstDayOfCurrentMonth;
        for (let i = 1; i <= firstDayOfCurrentMonth + 1; i++) {
            const tileConfig = this.createTileConfig(dayFromPreviousMonth, month - 1, year, '#F5F5F5', []);
            this.addEventsToDayTile(tileConfig, events, categories);
            tiles.push(tileConfig);
            dayFromPreviousMonth++;
        }
    }

    // create a tile for every day of the current month
    for (let i = 1; i <= daysInCurrentMonth; i++) {
        const tileConfig = this.createTileConfig(i, month, year, 'white', []);

        this.addEventsToDayTile(tileConfig, events, categories);

        tiles.push(tileConfig);
    }

    // add days from the next month until the viewport is filled
    if (tiles.length > 35) { // if it's over 35 tiles, anther row must be generated
        let i = 1;
        while (tiles.length !== 42) {
            const tileConfig = this.createTileConfig(i, month + 1, year, '', []);
            this.addEventsToDayTile(tileConfig, events, categories);
            tiles.push(tileConfig);
            i++;
        }
    } else {
        let i = 1;
        while (tiles.length !== 35) {
            const tileConfig = this.createTileConfig(i, month + 1, year, '', []);
            this.addEventsToDayTile(tileConfig, events, categories);
            tiles.push(tileConfig);
            i++;
        }
    }

    return tiles;
}

addEventsToDayTile(tileConfig: TileConfig, events, categories: Model.CategoryData[]) {
    if (!!events && !!events.length && !events.includes(undefined)) {
      events.forEach(event => {
        if (event.date.day === tileConfig.day
              && event.date.month === tileConfig.month
              && event.date.year === tileConfig.year) {

          categories.forEach(calendar => {
            if (calendar.name === event.category) {
              event = {
                ...event,
                color: calendar.color
              };
              // tileConfig.color = calendar.color;
              // console.log('event color:', event.color);
              // console.log('calendar color:', calendar.color);
            }
          });

          tileConfig.events.push(event);
        }
      });
    }
}

createTileConfig(day, month, year, color, events) {
    return {
      day: day,
      month: month,
      year: year,
      color: color,
      events: events,
    };
}

  get categoriesShowing(): string[] {
    return this.categories && this.categories.length
          ? this.categories
                  .filter(calendar => !!calendar.viewing)
                  .map(calendar => calendar.name)
                  : [];
  }
}
