import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges,
  Inject
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Store } from '@ngrx/store';
import * as Action from '@scheduling/store/actions';

  @Component({
    selector: 'app-day-modal',
    template: `
        <div mat-dialog-title fxLayout="column" fxLayoutAlign="center center">
          <h1 *ngIf="!creating" mat-dialog-title>{{ title | date }}</h1>
          <button mat-raised-button
            *ngIf="!creating"
            color="primary"
            class="button"
            (click)="toggleCreate()">
              Create New
           </button>
        </div>

        <div mat-dialog-content fxLayout="column" *ngIf="creating">
          <app-create-event mat-dialog-content
              [event]="data.event"
              [currentUser]="data.currentUser"
              [permissions]="data.permissions"
              [categories]="data.categories"
              (create)="createEvent($event)">
          </app-create-event>
        </div>

        <div mat-dialog-content fxLayout="column" *ngIf="!creating">
            <app-list-event mat-dialog-content
              [events]="data.events"
              [permissions]="data.permissions"
              [currentUser]="data.currentUser">
            </app-list-event>
        </div>

    `,
    styles: [`
      .button {
        width: 50vw;
      }
    `],
  })
  export class DayOfMonthModalComponent implements OnInit {

    title: any;
    creating: boolean;

    constructor(
      public dialogRef: MatDialogRef<DayOfMonthModalComponent>,
      public store: Store<any>,
      @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    ngOnInit() {
      if (!!this.data.events) { this.creating = true; }
      if (this.data.events.length > 0) { this.creating = false; }

      this.title = new Date(this.data.event.date.year, this.data.event.date.month, this.data.event.date.day).toDateString();
    }

    toggleCreate() {
      this.creating = !this.creating;
    }

    createEvent(event) {
      this.store.dispatch(new Action.CreateEvent(event));
      this.dialogRef.close();
    }

  }
