import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';


// RBC Forms
import { FieldConfig } from '@rbcForms/models';
import * as Field from '@scheduling/models/forms';
import * as Form from '@scheduling/models/forms/groups';

// My Imports
import * as Model from '@scheduling/models';
import * as UserModel from '@user/models';
import { AngularFirestore } from '@angular/fire/firestore';

// Support Modals That this Component Triggers
import { MatDialog, MatDialogConfig } from '@angular/material';
import { FormModalComponent } from '@rbcForms/components';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  @Input() currentUser: UserModel.UserData;
  @Input() currentUserId: string;
  @Input() categories: Model.CategoryData[];
  @Input() permissions: UserModel.Permissions;


  @Output() create = new EventEmitter<any>();
  @Output() update = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();

  constructor(
    private db: AngularFirestore,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {

  }

  toggleViewing(calendar) {
    const viewing = !calendar.viewing;
    const data = {
        viewing: viewing
    };
    this.update.emit({ id: calendar.id, data: data });
  }

  createCategory() {
    // create config for Modal
    const dialogConfig = new MatDialogConfig();

    // create formConfig that modal will use
    const form: FieldConfig[] = [
      Field.categoryName,
      Field.colorPicker,
    ];

    // add the title and submit text that the modal will use
    dialogConfig.data = {
        title: 'Create a Category',
        buttonSubmitText: 'Create Category',
        formConfig: form
    };

    // create a reference to the modal when it is opened
    const dialogRef = this.dialog.open(FormModalComponent, dialogConfig);

    // perform operations on the modal when it was closed based on the data it was closed with
    dialogRef
      .afterClosed()
      .subscribe(data => {
          const category: Model.CategoryData = {
            id: this.db.createId(),
            name: data.name,
            owner: this.currentUserId,
            color: data.color,
            viewing: true,
          };

          this.create.emit(category);
      });
  }

  updateCategory(category) {
    // create config for Modal
    const dialogConfig = new MatDialogConfig();

    // create the form fields and update their value with the chosen category
    let colorField = Field.colorPicker;
    colorField = {
      ...Field.colorPicker,
      value: category.color
    };

    // create formConfig that modal will use
    const form: FieldConfig[] = [
      colorField
    ];

    // add the title and submit text that the modal will use
    dialogConfig.data = {
        title: 'Change Category Color',
        buttonSubmitText: 'Update',
        formConfig: form
    };

    const dialogRef = this.dialog.open(FormModalComponent, dialogConfig);

    // perform operations on the modal when it was closed based on the data it was closed with
    dialogRef
      .afterClosed()
      .subscribe(data => {
          const updateData = {
            id: category.id,
            data: {
              color: data.color,
            }
          };

          this.update.emit(updateData);
      });
  }

  deleteCategory(calendar) {
    this.delete.emit(calendar);
  }

}
