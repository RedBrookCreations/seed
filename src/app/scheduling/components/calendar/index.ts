import { WeekComponent } from './week/week.component';
import { CategoriesComponent } from './categories/categories.component';
import { MonthComponent } from './month/month.component';
import { DayOfMonthModalComponent } from './month/day-of-month-modal.component';

// Handles Main Container View
export * from './week/week.component';
export * from './month/month.component';
export * from './month/day-of-month-modal.component';

// Handles Sidebar
export * from './categories/categories.component';

export const calendarComponents = [
    WeekComponent,
    MonthComponent,
    CategoriesComponent,
];

export const modalComponents = [
    DayOfMonthModalComponent,
];
