import { Routes } from '@angular/router';
import * as Container from './index';

export const ROUTES: Routes = [
    {
        path: 'calendar',
        component: Container.CalendarComponent
    },
    {
        path: 'view/:id',
        component: Container.EventComponent,
        data: {
            view: 'READ'
        }
    },
    {
        path: 'create',
        component: Container.EventComponent,
        data: {
            view: 'CREATE'
        }
    },
    {
        path: 'list',
        component: Container.EventsComponent,
        data: {
            view: 'LIST'
        }
    },
];
