export * from './calendar/calendar.container';
export * from './event/event.container';
export * from './events/events.container';

import { CalendarComponent } from './calendar/calendar.container';
import { EventComponent } from './event/event.container';
import { EventsComponent } from './events/events.container';

export const containers = [
    EventComponent,
    EventsComponent,
    CalendarComponent,
];

