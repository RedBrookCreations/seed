import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';

import { FieldConfig } from '@rbcForms/models';
import * as Field from '@scheduling/models/forms';

import * as Model from '@scheduling/models';
import * as UserModel from '@user/models';

import * as Action from '@scheduling/store/actions';
import * as CoreAction from '@core/store/actions';
import * as Selector from '@scheduling/store/selectors';
import * as UserStore from '@user/store';
import * as CoreStore from '@core/store';

import { AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: 'app-event',
  templateUrl: './event.container.html',
  styleUrls: ['./event.container.scss']
})
export class EventComponent implements OnInit {
  view$: Observable<string>;
  categories$: Observable<Model.CategoryData[]>;
  event$: Observable<Model.EventData>;
  permissions$: Observable<UserModel.Permissions>;

  constructor(
    private store: Store<any>,
    private db: AngularFirestore
  ) { }

  ngOnInit() {
    this.view$ = this.store.select(CoreStore.getPageView);
    this.permissions$ = this.store.select(UserStore.getPermissions);
    this.categories$ = this.store.select(Selector.getArrayOfCategorys);
    this.event$ = this.store.select(Selector.getSelectedEventFromRouteParams);
  }

  routeToEvent(event) {
    this.store.dispatch(new CoreAction.Go({
        path: [`events/view/${event.id}`]
    }));
  }

  createEvent(event) {
    console.log(event);
    this.store.dispatch(new Action.CreateEvent(event));
  }

  updateEvent(event) {
    console.log(event);
    this.store.dispatch(new Action.UpdateEvent(event));
  }

  deleteEvent(eventId) {
    console.log(eventId);
    this.store.dispatch(new Action.DeleteEvent(eventId));
  }

  onBack() {
    this.store.dispatch(new CoreStore.Back());
  }

}

