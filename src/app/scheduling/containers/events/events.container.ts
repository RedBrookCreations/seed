import { Component, OnInit } from '@angular/core';

/* NGRX & RXJS */
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
    concatMap,
    map,
    switchMap,
    tap,
    filter,
    delay,
    combineLatest,
    pluck,
    withLatestFrom,
    distinctUntilChanged
} from 'rxjs/operators';

/* My Imports */
import * as Model from '@scheduling/models';
import * as UserModel from '@user/models';

import * as Action from '@scheduling/store/actions';
import * as CoreAction from '@core/store/actions';

import * as Selector from '@scheduling/store/selectors';
import * as UserSelector from '@user/store/selectors';
import * as CoreSelector from '@core/store/selectors';
import { AngularFireAuth } from '@angular/fire/auth';


@Component({
  selector: 'app-events',
  templateUrl: './events.container.html',
  styleUrls: ['./events.container.scss']
})
export class EventsComponent implements OnInit {
  filters$: Observable<any>;
  events$: Observable<Model.EventData[]>;
  events: Model.EventData[] = null;
  currentUser$: Observable<UserModel.UserData>;

  loading = true;

  constructor(
    private store: Store<any>,
    private af: AngularFireAuth
  ) { }

  ngOnInit() {
    this.filters$ = this.store.select(CoreSelector.getPageFilters);
    this.events$ = this.store.select(Selector.getArrayOfEvents);
    this.currentUser$ = this.store.select(UserSelector.getCurrentUser);

    this.currentUser$
        .pipe(
            delay(200),
            tap(() => this.store.dispatch(new Action.GetEvents)),
            delay(200),
        )
        .subscribe(() => this.loading = false);
  }

  routeToEvent(event) {
      this.store.dispatch(new CoreAction.Go({
          path: [`events/view/${event.id}`]
      }));
  }

  onBack() {
    this.store.dispatch(new CoreAction.Back());
  }
}
