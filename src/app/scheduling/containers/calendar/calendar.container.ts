import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AngularFirestore } from '@angular/fire/firestore';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

// Seed Imports

import { DayOfMonthModalComponent } from '@scheduling/components/calendar/month/day-of-month-modal.component';


// Store
import * as Selector from '@scheduling/store/selectors';
import * as UserSelector from '@user/store/selectors';

import * as Action from '@scheduling/store/actions';
import * as UserStore from '@user/store';
import * as CoreStore from '@core/store';
import * as CoreAction from '@core/store/actions';

// Models
import * as Model from '@scheduling/models';
import * as UserModel from '@user/models';

// RBC Forms
import { FieldConfig } from '@rbcForms/models';
import * as Field from '@scheduling/models/forms';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.container.html',
  styleUrls: ['./calendar.container.scss']
})
export class CalendarComponent implements OnInit {

  @Input() fromOtherFeature: boolean;

  permissions$: Observable<UserModel.Permissions>;
  events$: Observable<Model.EventData[]>;
  categories$: Observable<Model.CategoryData[]>;
  currentUserId$: Observable<string>;
  currentUser$: Observable<UserModel.UserData>;

  drawerMode = 'side';
  drawerOpen = false;

  month: number;
  day: number;
  year: number;
  title: string;

  toggled: boolean;
  currentView: 'Month' | 'Day' | 'Week';
  viewSelectForm: FieldConfig[] = [
    Field.viewType
  ];

  constructor(
    private store: Store<any>,
    private db: AngularFirestore,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.month = new Date().getMonth();
    this.year = new Date().getFullYear();
    this.day = new Date().getDate();

    this.currentView = 'Month';
    this.setCurrentTitle(this.month);

    this.store.dispatch(new Action.GetEvents());
    this.store.dispatch(new Action.GetCategories());

    if (!this.fromOtherFeature) {
      this.store.dispatch(new CoreAction.SetCurrentFeature('Calendar'));
    }

    this.permissions$ = this.store.select(UserSelector.getPermissions);
    this.events$ = this.store.select(Selector.getArrayOfEvents);
    this.categories$ = this.store.select(Selector.getArrayOfCategorys);
    this.currentUser$ = this.store.select(UserSelector.getCurrentUser);
    this.currentUserId$ = this.store.select(UserSelector.getCurrentUserId);
  }

  changeView(value) {
    this.currentView = value.get('currentView').value;
  }

  toggleDrawer() {
    this.drawerOpen = !this.drawerOpen;
  }

  changeMonth(event) {
    this.month = event.getMonth();
    this.year = event.getFullYear();
    this.setCurrentTitle(this.month);
  }

  previousMonth() {
    if (this.month !== 0) {
      this.month--;
    } else {
      this.month = 11;
      this.year--;
    }
    this.setCurrentTitle(this.month);
  }

  nextMonth() {
    if (this.month !== 11) {
      this.month++;
    } else {
      this.month = 0;
      this.year++;
    }
    this.setCurrentTitle(this.month);
  }

  setCurrentTitle(month: number) {
    // tslint:disable-next-line:max-line-length
    const months = ['January', 'Feburary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    this.title = `${months[month]} ${this.year}`;
  }

  chooseDay(dayConfig) {
    const dialogRef = this.dialog.open(DayOfMonthModalComponent, {
      data: { ...dayConfig }
    });
  }

  chooseEvent(event) {
    this.store.dispatch(new CoreAction.Go({ path: [`/events/view/${event.id}`]}));
  }

  createCategory(categoryData: Model.CategoryData) {
      this.store.dispatch(new Action.CreateCategory(categoryData));
  }

  updateCategory(categoryData) {
    console.log(categoryData);
    this.store.dispatch(new Action.UpdateCategory(categoryData));
  }

  deleteCategory(categoryData) {
    console.log(categoryData);
    this.store.dispatch(new Action.DeleteCategory(categoryData.id));
  }

  addEvent() {
      this.store.dispatch(new CoreStore.Go({
        path: ['/events/create']
      }));
  }

  onBack() {
    this.store.dispatch(new CoreStore.Back());
  }

}
