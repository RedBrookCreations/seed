var SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');

module.exports = {
	navigateFallback: '/index.html',
	navigateFallbackWhitelist: [],
	root: 'dist/seed',
	plugins: [
		new SWPrecacheWebpackPlugin({
			cacheId: 'ng-pwa',
			filename: 'service-worker.js',
			staticFileGlobs: [
				'dist/index.html',
				'dist/**.js',
				'dist/**.css'
			],

		})
	],
	mergeStaticsConfig: true
};